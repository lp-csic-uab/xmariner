#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
xmr_help_01
(xmr_15)  28 de abril de 2008
"""
#
#
import wx
#
class HelpFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        wx.Frame.__init__(self, *args, **kwds)
        self.SetTitle("Help")
        self.SetSize((420, 430))
        txt = open('TODO_XMAR','r').read()
        self.txt = wx.TextCtrl(self, -1, style=wx.TE_MULTILINE)
        self.txt.AppendText(txt)
        self.txt.SetInsertionPoint(1)
        self.txt.Refresh()
        
class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        helpframe = HelpFrame(None)
        self.SetTopWindow(helpframe)
        helpframe.Show()
        return 1

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
