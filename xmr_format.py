#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_format_10
(xmr_15)28 de abril de 2008
"""
#

HTMLHEAD = """<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 9">
<meta name=Originator content="Microsoft Word 9">
<link rel=File-List href="./OB_word_archivos/filelist.xml">
<title>File</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>usuari</o:Author>
  <o:LastAuthor>Joaquin Abian</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>0</o:TotalTime>
  <o:Created>2008-02-24T16:15:00Z</o:Created>
  <o:LastSaved>2008-02-24T16:15:00Z</o:LastSaved>
  <o:Pages>2</o:Pages>
  <o:Words>358</o:Words>
  <o:Characters>2045</o:Characters>
  <o:Lines>17</o:Lines>
  <o:Paragraphs>4</o:Paragraphs>
  <o:CharactersWithSpaces>2511</o:CharactersWithSpaces>
  <o:Version>9.2812</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:HyphenationZone>21</w:HyphenationZone>
 </w:WordDocument>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;
	text-underline:single;}
p.style0, li.style0, div.style0
	{mso-style-name:style0;
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.xl30, li.xl30, div.xl30
	{mso-style-name:xl30;
	mso-style-parent:style0;
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	text-align:center;
	mso-pagination:widow-orphan;
	border:none;
	mso-border-alt:solid windowtext .5pt;
	padding:0cm;
	mso-padding-alt:0cm 0cm 0cm 0cm;
	font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	font-weight:bold;}
p.xl29, li.xl29, div.xl29
	{mso-style-name:xl29;
	mso-style-parent:style0;
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	text-align:center;
	mso-pagination:widow-orphan;
	border:none;
	mso-border-alt:solid windowtext .5pt;
	padding:0cm;
	mso-padding-alt:0cm 0cm 0cm 0cm;
	font-size:8.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.xl28, li.xl28, div.xl28
	{mso-style-name:xl28;
	mso-style-parent:style0;
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	text-align:center;
	mso-pagination:widow-orphan;
	border:none;
	mso-border-alt:solid windowtext .5pt;
	padding:0cm;
	mso-padding-alt:0cm 0cm 0cm 0cm;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.xl27, li.xl27, div.xl27
	{mso-style-name:xl27;
	mso-style-parent:style0;
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	text-align:center;
	mso-pagination:widow-orphan;
	border:none;
	mso-border-left-alt:solid windowtext .5pt;
	mso-border-bottom-alt:solid windowtext .5pt;
	mso-border-right-alt:solid windowtext .5pt;
	padding:0cm;
	mso-padding-alt:0cm 0cm 0cm 0cm;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.xl26, li.xl26, div.xl26
	{mso-style-name:xl26;
	mso-style-parent:style0;
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	text-align:center;
	mso-pagination:widow-orphan;
	border:none;
	mso-border-left-alt:solid windowtext .5pt;
	mso-border-right-alt:solid windowtext .5pt;
	padding:0cm;
	mso-padding-alt:0cm 0cm 0cm 0cm;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.xl25, li.xl25, div.xl25
	{mso-style-name:xl25;
	mso-style-parent:style0;
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	text-align:center;
	mso-pagination:widow-orphan;
	border:none;
	mso-border-top-alt:solid windowtext .5pt;
	mso-border-left-alt:solid windowtext .5pt;
	mso-border-right-alt:solid windowtext .5pt;
	padding:0cm;
	mso-padding-alt:0cm 0cm 0cm 0cm;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
p.xl24, li.xl24, div.xl24
	{mso-style-name:xl24;
	mso-style-parent:style0;
	margin-right:0cm;
	mso-margin-top-alt:auto;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	text-align:center;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1
	{size:841.9pt 595.3pt;
	mso-page-orientation:landscape;
	margin:70.9pt 42.55pt 2.0cm 42.55pt;
	mso-header-margin:35.45pt;
	mso-footer-margin:35.45pt;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
-->
</style>
</head>"""
#
#
#
CABECERA = """<body lang=ES link=blue vlink=purple style='tab-interval:35.4pt'>

<div class=Section1>

<div align=center>

<table border=0 cellspacing=0 cellpadding=0 width=946 style='width:709.25pt;
 margin-left:-10.55pt;border-collapse:collapse;mso-padding-alt:0cm 0cm 0cm 0cm'>
 <tr style='height:25.5pt'>
  <td width=100 style='width:75.25pt;border:solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;
  height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>File<o:p></o:p></span></b></p>
  </td>
  <td width=223 style='width:167.4pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>Protein
  Name<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.25pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>accession<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.3pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>species<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.25pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>MOWSE
  score<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.3pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>Theoretical
  Mr (Da)<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.25pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>Theoretical
  pI<o:p></o:p></span></b></p>
  </td>
  <td width=75 style='width:56.3pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>Peptide
  cover<o:p></o:p></span></b></p>
  </td>
  <td width=57 style='width:42.95pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>start-end<o:p></o:p></span></b></p>
  </td>
  <td width=57 style='width:43.0pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>mass<o:p></o:p></span></b></p>
  </td>
  <td width=57 style='width:43.0pt;border:solid windowtext .5pt;border-left:
  none;padding:.75pt .75pt 0cm .75pt;height:25.5pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><b><span lang=EN-GB style='mso-ansi-language:EN-GB'>error (ppm)<o:p></o:p></span></b></p>
  </td>
 </tr>"""
#
#Este modulo se repite para cada file
#rowspan = #matchesprot1 + #matchesprot1
# MALDI %(rowspan,filename)
#
MALDI = """ <tr style='height:12.75pt'>
  <td width=100 rowspan=%i style='width:75.25pt;border:solid windowtext .5pt;
  border-top:none;padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><span lang=EN-GB style='mso-ansi-language:EN-GB'>%s<o:p></o:p></span></p>
  </td>"""
#
#Este modulo se repite para cada hit en cada file
#rowspan = #matchesprot(i) <--varquim  (usarlo para -->
#a) PROTEIN.replace('varquim', rowspan)
#b) PROTEIN %(protein_name, accession, mowse, mwpi, cover)
#
PROTEIN = """ 
  <td width=223 rowspan=varquim style='width:167.4pt;border-top:none;border-left:
  none;border-bottom:solid black .5pt;border-right:solid windowtext .5pt;
  mso-border-left-alt:solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;
  height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><span lang=EN-GB style='mso-ansi-language:EN-GB'>%s<o:p></o:p></span></p>
  </td>
  <td width=75 rowspan=varquim style='width:56.25pt;border-top:none;border-left:none;
  border-bottom:solid black .5pt;border-right:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'>%s</p>
  </td>
  <td width=75 rowspan=varquim style='width:56.3pt;border-top:none;border-left:none;
  border-bottom:solid black .5pt;border-right:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'>%s</p>
  </td>
  <td width=75 rowspan=varquim style='width:56.25pt;border-top:none;border-left:none;
  border-bottom:solid black .5pt;border-right:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'>%s</p>
  </td>
  <td width=75 rowspan=varquim style='width:56.3pt;border-top:none;border-left:none;
  border-bottom:solid black .5pt;border-right:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'>%s</p>
  </td>
  <td width=75 rowspan=varquim style='width:56.25pt;border-top:none;border-left:none;
  border-bottom:solid black .5pt;border-right:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'>%s</p>
  </td>
  <td width=75 rowspan=varquim style='width:56.3pt;border-top:none;border-left:none;
  border-bottom:solid black .5pt;border-right:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'>%s</p>
  </td>
"""
#
#
#Este modulo es el primer match en cada hit
MATCH_HEAD=  """
  <td width=57 style='width:42.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt;
  padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:8.0pt'>%s<o:p></o:p></span></p>
  </td>
  <td width=57 style='width:43.0pt;border-top:none;border-left:none;border-bottom:
  solid windowtext .5pt;border-right:solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;
  height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:8.0pt'>%.2f<o:p></o:p></span></p>
  </td>
  <td width=57 style='width:43.0pt;border-top:none;border-left:none;border-bottom:
  solid windowtext .5pt;border-right:solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;
  height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:8.0pt'>%.2f<o:p></o:p></span></p>
  </td>
 </tr>
"""
#
#
#Este modulo se repite para cada uno de los mathes restantes en cada hit
MATCHES = """
 <tr style='height:12.75pt'>
  <td width=57 style='width:42.95pt;border-top:none;border-left:none;
  border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt;
  padding:.75pt .75pt 0cm .75pt;height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:8.0pt'>%s<o:p></o:p></span></p>
  </td>
  <td width=57 style='width:43.0pt;border-top:none;border-left:none;border-bottom:
  solid windowtext .5pt;border-right:solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;
  height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:8.0pt'>%.2f<o:p></o:p></span></p>
  </td>
  <td width=57 style='width:43.0pt;border-top:none;border-left:none;border-bottom:
  solid windowtext .5pt;border-right:solid windowtext .5pt;padding:.75pt .75pt 0cm .75pt;
  height:12.75pt'>
  <p class=MsoNormal align=center style='margin:0cm;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:8.0pt'>%.2f<o:p></o:p></span></p>
  </td>
 </tr>
"""
#
#la siguiente proteina de un maldi tiene un inicio de tabla
#
NEXT_PROT="""
 <tr>
"""
#
#
#
FINAL_TABLE = """
</table>

</div>
"""
#
#
#Entre paginas
#
PAGES ="""<p class=MsoNormal align=center style='text-align:center'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>
"""
#
#
#
FINAL = """
<p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>

</div>

</body>

</html>
"""