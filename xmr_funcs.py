#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_funcs_11
(xmr_15)  28 de abril de 2008
"""
#
import os, sys
import time
import wx
import urllib
import traceback
from cStringIO import StringIO
from xmr_format_10 import HTMLHEAD, FINAL
from xmr_cls_hit_03 import HitScanner, NullProteinHit, htmltags, dictags
from xmr_cls_espectro_02 import Spectro
#
#
value = {'parent_masses':"""676.3718
825.4571
1017.5370
1026.5964
1040.6121
1105.4550
1196.7132
1218.5796""",
'max_reported_hits' : '10',
'tolerance_units': 'ppm', 
'parent_mass_tolerance': '100',	
'cys':'carbamidomethylation',
'species': 'MAMMALS',
'mowse_on': '1',
'database':'SwissProt.fasta'}
#
#
def get_filepath(win, defdir=None, isdir=False):
    """ dialogos para abrir files cuyo path aparece en un textcontrol"""
    
    defaultdir = defdir if defdir else os.getcwd()
    try:
        archivo_actual = win.GetValue()
    except AttributeError:
        archivo_actual = None
        
    if archivo_actual:
        defaultdir = archivo_actual 
    #
    if isdir:
        fd = wx.DirDialog(None, defaultPath=defaultdir)
    else:
        fd = wx.FileDialog(None, defaultDir=defaultdir)
    
    if fd.ShowModal() == wx.ID_OK:
        flpth = fd.GetPath()
        try:
            win.SetLabel(flpth)
        except AttributeError:
            pass
        fd.Destroy()
    else:
        fd.Destroy()
        return None, None
    #
    if not os.path.exists(flpth):
        if dame_ok('File Nueva. Crear?'): open(flpth,'w').write('')
        return flpth, False
    else:
        return flpth, True
#
#
def dame_ok(pregunta, titulo='Pregunta'):
    """
    Ventana de dialogo para cosas generales, OK y CANCEL
    Pregunta puede ser una tupla de lineas o un texto
    """
    if type(pregunta) is type((1,)):
        text = ''
        for item in pregunta:
            text += item + '\n'
    else:
        text = pregunta
    
    dialog = wx.MessageDialog(None, text, titulo,
                                style=wx.OK|wx.CANCEL|wx.ICON_QUESTION)
    if dialog.ShowModal() == wx.ID_OK:
        dialog.Destroy()
        return True
    return False
#
#
def aviso(warning):
    "mensaje de aviso"
    dlg = wx.MessageDialog(None, warning, style=wx.OK)
    if dlg.ShowModal() == wx.ID_OK:
        dlg.Destroy()
#
#
def get_query(value, server=None):
    "hace un GET al servidor y recibe la respuesta"
    if not server : server = "http://158.109.210.59/ucsfbin3.2/msfit.cgi?"
    params = urllib.urlencode(value)
    #
    try:
        f = urllib.urlopen(server + '%s' %params)
        log = 'Servidor OK'
    except IOError:
        log = 'Servidor sin Conexion'
        return None, log
    html = f.read()
    f.close()
    return html, log 
#
#
def preformat_report(lista_maldis, n_match, minscore, maxdif, log):
    ""
    pages = []
    new_lista_maldis = []
    total_rows = 0
    #
    for maldi in lista_maldis:          #lista maldis de un usuario
        new_maldi = []
        maldihits = maldi.hits[:n_match]    #numero de candidatos por maldi
        already_one = False
        for protein in maldihits:
            protein_rowspan = len(protein.mass)
            mowse = protein.mowse
            score = float(mowse)
            #
            if score < minscore:
                if already_one : break
                protein_rowspan = 1
                _prot = NullProteinHit(protein.fname, mowse)
            elif already_one:
                dif = (previous_score/score if (previous_score > score)
                       else score/previous_score)
                if dif > maxdif: break
            _prot = protein
             
            previous_score = score
            #
            total_rows += protein_rowspan
            
            if total_rows > 30:
                if new_maldi:
                    new_lista_maldis.append(new_maldi)
                pages.append(new_lista_maldis)
                if already_one:
                    pass
                    #deberia marcarse que es una continuacion
                new_maldi = [_prot]
                new_lista_maldis = []
                total_rows = protein_rowspan
            else:
                new_maldi.append(protein)
            
            already_one = True
        new_lista_maldis.append(new_maldi)
    pages.append(new_lista_maldis)
    
    page_number = len(pages)
    idx = 0
    txt = ""
    for page in pages:
        idx += 1
        txt += format_report(page, log)
        if idx < page_number : txt += PAGES
        
    return HTMLHEAD + txt + FINAL
#
#
def format_report(page, log=sys.stdout.write):
    """
    extrae los datos alacenados en diccio y los carga dentro de la
    extructura de texto de excel (archivo .htm)
    
    HTMLHEAD
    CABECERA
    MALDI  ----> Se repite para cada file
                rowspan = #matchesprot1 + #matchesprot2
                MALDI %(rowspan,filename)
    PROTEIN --> Se repite para cada hit en cada file
                rowspan = #matchesprot(i) <--varquim
                a) PROTEIN.replace('varquim', rowspan)
                b) PROTEIN %(protein_name, accession, mowse, mwpi, cover)
    MATCH_HEAD
    MATCHES --> Se repite para cada uno de los matches restantes en cada hit
    FINAL
    
    n_macht es el m�ximo n�mero de prote�nas que se reportan
    """
   
    maldi_rowspan = 0
    #
    PROTEINTXTFULL = []
    #
    for maldi in page: #maldis de un usuario
        maldi_rowspan = 0
        txt = ''
        already_one = False
        for protein in maldi:
            protein_rowspan = len(protein.mass)
            mowse = protein.mowse
            maldi_rowspan += protein_rowspan
            protein_name = protein.prot_name
            accession = protein.accesion
            especie = protein.species
            mw = protein.pmw
            pi = protein.ppi
            cover = protein.cover
            PROTEIN2 = PROTEIN.replace('varquim', str(protein_rowspan))
            if already_one: txt += NEXT_PROT
            txt += PROTEIN2 % (protein_name, accession, especie, mowse, mw, pi, cover)
            already_one = True
            
            for i in range(protein_rowspan):
                datos = (protein.startend[i],
                         float(protein.mass[i]), float(protein.error[i]))
                if i:
                    txt += MATCHES % datos
                else:
                    txt += MATCH_HEAD % datos
        
        name = corta_nombre(protein.fname)
        MALDITXT = MALDI % (maldi_rowspan, name)
        PROTEINTXT = MALDITXT + txt   
        PROTEINTXTFULL.append(PROTEINTXT)
    
    FULLPROTEIN = ''.join(PROTEINTXTFULL)    
    #
    return CABECERA + FULLPROTEIN + FINAL_TABLE
#
#
def corta_nombre(nombre):
    """
    separa en partes nombre largo de files sin separaciones (pero con '_')
    OB_1A_G4_b_0001.dat_r.htm
    """
    trozeado = []
    nombre = nombre.split('.')[0]
    cortes = nombre.split('_')
    cortes = ' '.join(cortes)
    atomos = cortes.split(' ')
    for atomo in atomos:
        if len(atomo) < 10:
            trozeado.append(atomo)
            continue
        ini = 0
        while ini < len(atomo):
            atomo = atomo[0:10+ini] + ' ' + atomo[ini+10:]
            ini += 10
        
        trozeado.append(atomo)
            
    return ' '.join(trozeado)
#
#
def runapp(archive, valor=value, n_match=2, minscore=100, maxdif=1000,
            dir_result='', dir_report='', run_servidor = False,
            filter=None, log=sys.stdout.write):
    ""
    result_list = []
    for maldifile in archive:
        if not os.path.isfile(maldifile):
            continue
        filename = os.path.basename(maldifile)
        username = filename.split('_')[0]
        name, ext = os.path.splitext(filename)
        savefile = dir_result + os.sep + name + '_r.htm'
        
        if run_servidor:
            try:
                espectro = Spectro(maldifile)
                if filter:
                    espectext = '\n'.join(espectro.get_best_masses(*filter))
                else:
                    espectext = '\n'.join(espectro.masas)
                valor['parent_masses'] = espectext
            except ValueError:
                log('Error Lectura espectro de ' + maldifile)
                continue
            htmltxt, error = get_query(valor, run_servidor)
            log(error)
            if htmltxt:
                log('savefile ' + savefile)
                open(savefile, 'w').write(htmltxt)
            else:
                log('Prospector devuelve resultado vacio de ' + maldifile)
                continue
        else:
            try:
                htmltxt = open(maldifile,'r').read()
            except IOError:
                log('ErrorLectura ' + maldifile)
                continue
                
        error_fatal = (KeyboardInterrupt, MemoryError)
                   
        try:
            hitbook = HitScanner(htmltxt, htmltags, dictags, filename)
            #if 0: check_atributos(lector)
        except error_fatal:
            raise
        except Exception:
            fle = StringIO()
            traceback.print_exc(file=fle)
            valor = fle.getvalue()
            log(valor)
        else:
            if hitbook.matches: result_list.append(hitbook)    
        
    if not result_list:
        log('No se genera archivo resultados de ' + str(archive))
        return None
    
    htmltxt = preformat_report(result_list, n_match, minscore, maxdif, log)
    fechayhora = time.localtime(time.time())
    date = '%02d%02d%02d' % tuple(fechayhora[0:3])
    savefile_name = '%s_%s_%s' % (username, len(archive), date[2:])
    savebase = dir_report + os.sep + savefile_name 
    savehtm = savebase + '.htm'
    #
    n = 1
    while os.path.exists(savehtm):
        savehtm = savebase + '_' + str(n) + '.htm'
        n += 1
    #
    n = 5
    while n > 0:
        try:
            open(savehtm, 'w').write(htmltxt)
            return savehtm
        except IOError:
            n -= 1
    return None   
#
#
def calcula_coste(muestras, tipo, tramos):
    """
    tramos = [[5, 28, 30, 40], [5, 35, 40 ,60], ...]
    tipo = 1|2|3  -> indices de la lista
    querria general -> [[5,28], [5, 35], [2, 40]]
    """
    resumen = []
    n = muestras = int(muestras)
    tipo = tipo + 1
    #
    try:
        mxtramo = int(tramos[0][0])
    except (IndexError, ValueError):
        return [[0, 0]]
    #
    if not tipo: return [[0, 0]]
    #
    multip = float(tramos[0][tipo])
    quedan = n - mxtramo
    idx = 0
    if quedan > 0:
        while not quedan < 1:
            nsave = quedan
            resumen.append([mxtramo, multip])
            idx += 1
            multip = float(tramos[idx][tipo])
            mxtramo = int(tramos[idx][0])
            quedan -= mxtramo
        resumen.append([nsave, multip])
    else:
        resumen.append([muestras, multip])
        
    #comprimir a 3:
    if len(resumen) > 2:
        samplesxavg = 0
        costxavg = 0
        for i in range(2, len(resumen)):
            samples = resumen[i][0]
            samplesxavg += samples
            cost = resumen[i][1]
            costxavg += samples * cost
        cost_avg = costxavg / samplesxavg
        resumen_final = resumen[0:2] + [[samplesxavg, cost_avg]]
    else:
        resumen_final = resumen
        
    return resumen_final
#
#
def genera_txt_costes(lista):
    ""
    txt = ''
    
    if len(lista) == 1:
        coste = lista[0][0] * lista[0][1]        
        txt = "%i muestras a %.1f euros/muestra = %.1f Euros" % (
                        lista[0][0], lista[0][1], coste)
    
    if len(lista) == 2:
        coste = lista[0][0]*lista[0][1] + lista[1][0]*lista[1][1]
        txt = """%i primeras muestras a %.1f euros/muestra,
        resto a %.1f euros/muestra, total %.1f""" % (
        lista[0][0], lista[0][1], lista[1][1], coste
        )
        
    if len(lista) > 2:
        coste = float(lista[0][0] * lista[0][1] +
                      lista[1][0] * lista[1][1] + lista[2][0] * lista[2][1])
        txt = """%i primeras muestras a %.1f euros/muestra,
        %i siguientes muestras a %.1f euros/muestra,
        resto, a %.1f euros/muestra, total %.1f""" % (
        lista[0][0], lista[0][1],
        lista[1][0], lista[1][1],
        lista[2][1], coste)

    return txt
        
 
if __name__ == "__main__":
    pass
    check = [0,              #calculacoste
             1               #dame_ok            
            ]    
    #
    #calculacoste
    if check[0]:        
        tramos = [[5, 28, 30, 40], [5, 20, 25, 30],
                  [10, 18, 20, 25], [10, 15, 18, 20], [100, 10, 112, 15]]
        muestras = [4, 8, 14, 50]
        for numero in muestras:
            lista = calcula_coste(numero, 1, tramos)
            
            print '\n', numero, '\n', genera_txt_costes(lista)
    #
    #dameok
    if check[1]:
        app = wx.PySimpleApp()
        OK = dame_ok('hola?', 'Test texto')
        if OK:
            print 'hola'
        else:
            print 'adios'
            
        OK = dame_ok(('hola', 'que tal'), 'Test tupla')
        if OK:
            print 'hola'
        else:
            print 'adios'