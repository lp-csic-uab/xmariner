#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_cls_espectro_02
(xmr_14) 29 de abril de 2008
"""
import numpy
from scipy import stats

class Spectro():
    def __init__(self, archivo):
        self.spectro = None
        self.inten = []
        self.masas = []
        self.set_maldispect(archivo)
        
    def stats(self):
        intensities = [inten for masa, inten in self.spectro]
        return self.calc_statistics(intensities)
    
    def set_maldispect(self, arch):
        """lee el espectro de una file.
        esta fijado para las files del MALDI (empiezan en la cuarta linea y
        no hay nada despues de la lista del espectro
        admite listas de masas sin intensidad
        """
        filetext = open(arch, 'r').read()
        listtxt = [txtline.split() for txtline in filetext.splitlines()]
        listspec = listtxt[3:]
        if len(listspec) == 1:
            self.spectro = [(masa, 0) for masa, inten in listspec]
            self.masas = listspec
        else:
            self.spectro = [(masa, float(inten)) for masa, inten in listspec]
            self.masas = [masa for masa, inten in listspec]
        
    def get_best_masses(self, n=5, size=100):
        """esta funcion debe coger de cada sector de tama�o "size"
        un maximo de los "n" mas altos. 
        """
        limit = 800
        section = []
        selected = []
        #
        for masa, inten in self.spectro:
            if float(masa) > limit:
                while float(masa) > limit:
                    limit += size
                selected.extend(sorted(section, reverse=True)[:n])
                section = []

            section.append((inten, masa))
        selected.extend(sorted(section, reverse=True)[:n])
        #
        return [mass for int, mass in selected]
    
    def check(self, lista=None):
        """lista : listado de iones utilizados para el hit
        deberia devolver un valor que determinara si la lista es
        relevante -> si explica un buen numero de las senales mayoritarias
        """
        lista_hits = [inten for masa, inten in self.spectro if masa in lista]
        lista_nohits = [inten for masa, inten in self.spectro if masa not in lista]
                
        return self.calc_statistics(lista_hits), self.calc_statistics(lista_nohits)
    
    def calc_statistics(self, listado):
        """cuando se hace una busqueda se valora cuantos iones del total
        se han utilizado pero no se tiene en cuenta si esos iones son ruido
        o no, es decir su intensidad relativa
        """
        intensities = numpy.array(listado)
        return stats.stats.describe(intensities)

        
def _test_estadistica(espectro, archivo, hits):
    from pylab import hist, plot, hold, show, bar, text
    hold(False)
    params = ['n ', 'min-max ', 'media ', 'varianza ', 'skew ', 'kurt ']
    intensities = [inten for masa, inten in espectro.spectro]
    n, bins, w = hist(intensities, 100)
    estadistica = espectro.stats()
    est_hit, est_nohit = espectro.check(hits)
    #
    rango = estadistica[1]
    y = n[0]
    x = rango[0] + 0.2*(rango[1] - rango[0])
    #
    for param, item, item2, item3 in zip(params, estadistica, est_hit, est_nohit):
        y -= 0.7
        texto = '%s  %s  %s' %(str(item), str(item2), str(item3))
        text(x, y, param + texto)
    #
    tabla = [(float(mass), inten) for mass, inten in espectro.spectro]
    tabla.sort()
    xx = [x for x, y in tabla]
    yy = [y for x, y in tabla]
    show()
    bar(xx, yy)
    show()
    
    while True:
        mas = raw_input('Mas? Y/N = ')
        if mas: break
        

if __name__ == '__main__':
    
    archivo = 'test/espectro.txt'
    hits = ['1693.030518', '905.525391', '1569.855103']
    espectro = Spectro(archivo)
    _test_estadistica(espectro, archivo, hits)
    