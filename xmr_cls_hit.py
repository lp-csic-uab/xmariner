#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
xmr_cls_hit_03
(xmr_15)  28 de abril de 2008
"""
#
from xmr_cls_reader_01 import HtmlReader, htmltags, dictags
#
value = {'parent_masses':"""676.3718
825.4571
1017.5370
1026.5964
1040.6121
1105.4550
1196.7132
1218.5796""",
'max_reported_hits' : '10',
'tolerance_units': 'ppm', 
'parent_mass_tolerance': '100',	
'cys':'carbamidomethylation',
'species': 'MAMMALS',
'mowse_on': '1',
'database':'SwissProt.fasta'}
#
#
class HitScanner(HtmlReader):
    """ Clase Hit.
    Lector contiene los datos producidos por protein prospector ya extraidos
    del html.
    """
    def __init__(self, html, tags, dictags, fname=''):
        ""
        HtmlReader.__init__(self, html, tags, dictags, fname)
        self.hits = []
        match_prot = []
        #
        ini = 0
        pos = 0
        txt = ''
        #
        for idx, hits in enumerate(self.matches):
            hits = int(hits)
          
            params = (self.fname,
                      self.prot_name[idx], self.accesion[idx],
                      self.species[idx], self.mowse[idx],
                      self.pmw[idx], self.ppi[idx],
                      self.cover[idx])
           
            startend = []
            mass = []
            error = []
            repes = 0
            while (pos < ini+hits+repes):
                secmass = self.mass[pos]
                if secmass in mass : repes += 1
                mass.append(secmass)
                startend.append(self.startend[pos])
                error.append(self.error[pos])
                
                pos += 1
            #
            ini = pos
            #
            datos = (startend, mass, error)
            
            newinstance = ProteinHit(params+datos)
            self.hits.append(newinstance)
#
#
class ProteinHit():
    "define un hit/proteina con todos sus datos"
    def __init__(self, params):
        self.fname = params[0]
        self.prot_name = params[1]
        self.accesion = params[2]
        self.species = params[3]
        self.mowse = params[4]
        self.pmw = params[5]
        self.ppi = params[6]
        self.cover = params[7]
        self.startend = params[8]
        self.mass = params[9]
        self.error = params[10]
        
        if len(self.prot_name) > 110:
            self.prot_name = self.prot_name[:90] + ' ...more'
#
#
class NullProteinHit():
    "define un hit/proteina con todos sus datos"
    def __init__(self, fname, mowse):
        self.fname = fname
        self.prot_name = "None"
        self.accesion = ""
        self.species = ""
        self.mowse = mowse
        self.pmw = ""
        self.ppi = ""
        self.cover = ""
        self.startend = ['']
        self.mass = ['0']
        self.error = ['0']