#!/usr/bin/env python
import base64, quopri
import mimetypes
from email import Generator, Message
import cStringIO, os
import smtplib
# sample addresses
#toAddr = "mgmbam@iibb.csic.es"
toAddr = "Joaquim.Abian.csic@uab.cat"
fromAddr = "Joaquim.Abian.csic@uab.cat"
outputFile = "dirContentsMail"
archivo_datos = "C:/MP_3_080226_rep.pdf"

def main( ):
    mainMsg = Message.Message( )
    mainMsg["To"] = toAddr
    mainMsg["From"] = fromAddr
    mainMsg["Subject"] = "UN ROBOT TE ENVIA UN INFORME"
    mainMsg["Mime-version"] = "1.0"
    mainMsg["Content-type"] = "Multipart/mixed"
    mainMsg.preamble = "Mime message\n"
    mainMsg.epilogue = "\n" # to ensure that message ends with newline
        
    fileNames = [archivo_datos]
    
    for fileName in fileNames:
        contentType, ignored = mimetypes.guess_type(fileName)
        if contentType is None:     # If no guess, use generic opaque type
            contentType = "application/octet-stream"
        contentsEncoded = cStringIO.StringIO( )
        f = open(fileName, "rb")
        mainType = contentType[:contentType.find("/")]
        if mainType=="text":
            cte = "quoted-printable"
            quopri.encode(f, contentsEncoded, 1)   # 1 to also encode tabs
        else:
            cte = "base64"
            base64.encode(f, contentsEncoded)
        f.close( )
        subMsg = Message.Message( )
        subMsg.add_header("Content-type", contentType, name=fileName)
        subMsg.add_header("Content-transfer-encoding", cte)
        subMsg.set_payload(contentsEncoded.getvalue( ))
        contentsEncoded.close( )
        mainMsg.attach(subMsg)
    f = open(outputFile, "wb")
    g = Generator.Generator(f)
    g.flatten(mainMsg)
    f.close( )


def sendmail(arch):
    smtpserver = 'smtp.uab.cat'
    AUTHREQUIRED = 0        # if you need to use SMTP AUTH set to 1
    smtpuser = '1187714@pop.uab.cat'  # for SMTP AUTH, set SMTP username here
    smtppass = 'paris2007'  # for SMTP AUTH, set SMTP password here

    RECIPIENTS = [toAddr]
    SENDER = fromAddr
    
    mssg = open(arch, 'r').read()
   
    session = smtplib.SMTP(smtpserver)
    if AUTHREQUIRED:
        session.login(smtpuser, smtppass)
    smtpresult = session.sendmail(SENDER, RECIPIENTS, mssg)

    if smtpresult:
        errstr = ""
        for recip in smtpresult.keys():
            errstr = """Could not delivery mail to: %s
                        Server said: %s
                        %s
                        %s""" % (recip, smtpresult[recip][0],
                                 smtpresult[recip][1], errstr)
        raise smtplib.SMTPException, errstr

if __name__ == "__main__":
    main( )
    sendmail(outputFile)

					  