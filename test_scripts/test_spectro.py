from xmr_cls_espectro_02 import Spectro
archivo = 'test/espectro.txt'
espectro = Spectro(archivo)
best_masses = espectro.get_best_masses(5, 100)
print best_masses

for espmass in espectro.masas:
    for mass in best_masses:
        if espmass == mass:
            print mass