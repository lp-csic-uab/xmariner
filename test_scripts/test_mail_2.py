#!/usr/bin/env python
#

import smtplib, sys, MimeWriter, mimetypes, mimetools, base64
import os
import StringIO

SMTPSERVER = 'smtp.uab.cat'
AUTHREQUIRED = 0        # if you need to use SMTP AUTH set to 1
smtpuser = '1187714@pop.uab.cat'  # for SMTP AUTH, set SMTP username here
smtppass = 'paris2007'  # for SMTP AUTH, set SMTP password here

toAddr = "Joaquim.Abian.csic@uab.cat"
fromAddr = "Joaquim.Abian.csic@uab.cat"
outputFile = "dirContentsMail"
attach = ["C:\\MP_3_080226_rep.pdf"]

def mail(sender='', to='', subject='', text='', attachments=None):
    """
    Usage:
        mail()
    Params:
        sender: sender's email address
        to: receipient email address
        text: Email message body main part.
        attachments: list of files to attach
    """
    message = StringIO.StringIO()
    writer = MimeWriter.MimeWriter(message)
    writer.addheader('To', to)
    writer.addheader('From', sender)
    writer.addheader('Subject', subject)
    writer.addheader('MIME-Version', '1.0')
    
    writer.startmultipartbody('mixed')
    
    # start with a text/plain part
    part = writer.nextpart()
    body = part.startbody('text/plain')
    part.flushheaders()
    body.write(text)

    # now add the attachments
    if attachments is not None:
        for a in attachments:
            filename = os.path.basename(a)
            ctype, encoding = mimetypes.guess_type(a)
            if ctype is None:
                ctype = 'application/octet-stream'
                encoding = 'base64'
            elif ctype == 'text/plain':
                encoding = 'quoted-printable'
            else:
                encoding = 'base64'
                
            part = writer.nextpart()
            part.addheader('Content-Transfer-Encoding', encoding)
            body = part.startbody("%s; name=%s" % (ctype, filename))
            mimetools.encode(open(a, 'rb'), body, encoding)

    # that's all falks
    writer.lastpart()
    return message

def send_mail(sender, to, message):
    
    # send the mail
    smtp = smtplib.SMTP(SMTPSERVER)
    smtp.set_debuglevel(1)
    smtp.sendmail(sender, to, message.getvalue())
    smtp.quit()


if __name__ == "__main__":
    
    msg = mail(sender= fromAddr, to= toAddr,
              subject='ENVIO DE UN ROBOT',
              text='RECIBE EL DOCUMENTO.\n\n',
              attachments=attach)
    
    send_mail(fromAddr, toAddr, msg)
