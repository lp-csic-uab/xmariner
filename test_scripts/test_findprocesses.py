#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
Permite ver que procesos estan activos
Puede servir para evitar que se estropee el pdf
"""

import win32pdh

def print_objects():
    object = win32pdh.EnumObjects(None, None, -1)
    for item in object:
        print item

def print_items(object):
    processes = win32pdh.EnumObjectItems(None, None, object, -1)
    for proces in processes:
        for item in proces:
            print item
    
print ""
print_items('Proceso')
print "\n**** Objects  ****\n"
print_objects()


##% de tiempo de procesador
##% de tiempo de usuario
##% Tiempo privilegiado
##Uso m�ximo de espacio virtual
##Bytes del espacio virtual
##Errores de p�gina/s.
##Uso m�ximo del espacio de trabajo
##Espacio de trabajo
##Uso m�ximo de los bytes del archivo de p�ginas
##Bytes del archivo de p�ginas
##Bytes privados
##N�mero de subprocesos (subprocesos)
##Prioridad base
##Tiempo transcurrido
##Identificador del proceso
##Creando Id. de proceso
##Bytes de bloque paginado
##Bytes de bloque no paginado
##Cantidad de manipuladores
##N�m. de operaciones de lectura de E/S por seg.
##N�m. de operaciones de escritura de E/S por seg.
##Operaciones de datos de E/S por seg.
##Otras operaciones de E/S por seg.
##N�m. de bytes le�dos E/S por seg.
##N�m. de bytes escritos E/S por seg.
##Bytes de datos E/S por seg.
##Otros Bytes de E/S por seg.
##Idle
##System
##smss
##csrss
##winlogon
##services
##lsass
##svchost
##svchost
##svchost
##svchost
##spoolsv
##alg
##httpd
##NTRtScan
##httpd
##snmp
##TmListen
##CNTAoSMgr
##CAA38A
##explorer
##jusched
##PDVDServ
##TSVNCache
##carpserv
##qttask
##ONETOUCH
##atiptaxx
##PccNTMon
##ctfmon
##msmsgs
##ApacheMonitor
##Belkinwcui
##ChkDev
##MSOFFICE
##firefox
##pythonw
##hh
##wmiprvse
##wmiprvse
##python
##wmiapsrv
##_Total 

##**** Objects  ****

##Datos de .NET CLR
##Red de .NET CLR
##Proveedor de datos de .NET para Oracle
##Proveedor de datos de .NET para SqlServer
##Memoria de .NET CLR
##Interoperabilidad de .NET CLR
##Excepciones de .NET CLR
##Carga de .NET CLR
##LocksAndThreads de .NET CLR
##Compilador JIT de .NET CLR
##Entorno remoto de .NET CLR
##Seguridad de .NET CLR
##ASP.NET
##Aplicaciones ASP.NET
##ASP.NET v2.0.50727
##ASP.NET Apps v2.0.50727
##Servicio de estado ASP.NET
##Coordinador de transacciones distribuidas de Microsoft
##DiscoL�gico
##DiscoF�sico
##Servidor
##Colas de trabajo del servidor
##Redirector
##Explorador
##Cach�
##Procesador
##Memoria
##Objetos
##Archivo de paginaci�n
##Sistema
##Proceso
##Subproceso o subproceso
##Objeto Trabajo
##Detalles del objeto de trabajo
##Canal de PSched
##Flujo de PSched
##Puerto RAS
##Total RAS
##Servicio RSVP
##Interfaces RSVP
##Cola de impresi�n
##Telefon�a
##Interfaz de red
##IP
##ICMP
##TCP
##UDP
##Sesi�n de Servicios de Terminal Server
##Servicios de Terminal Server
##Objetos de WMI
##BatteryStatus
##ProcessorPerformance
##Coordinador de transacciones distribuidas de Microsoft
##Espacio de direcciones del proceso
##Imagen
##Imagen completa
##Detalles de subprocesos
##Canal de PSched
##Flujo de PSched
##Puerto RAS
##Total RAS
##Servicio RSVP
##Interfaces RSVP
##Telefon�a
##Sesi�n de Servicios de Terminal Server
##Servicios de Terminal Server
##Script terminated.