#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_cls_reader_01.py
(xmr_15) 28 de abril de 2008
"""
#
htmltags= ['prot_name', 'accesion', 'mowse', 'pmw', 
           'ppi', 'cover', 'match', 'startend', 'mass', 'species']
        
dictags = dict( prot_name = ('><!--entry_name', '\n'),
                accesion = ('<A HREF =\\', '</A>'),
                mowse = ('<!--mowse_', '<TH'),
                pmw =   ('<!--protein_mw', ' /'),
                ppi =   ('<!--protein_pI', '<TD>'),
                cover = ('<!--protein_cover', '</B>'),
                match = ('<!--fraction_match', 'match'),
                startend = ('<!--delta_mass', '<TH AL'),
                species =  ('><!--species','<TH>'),
                mass =  ('><!--in_mass', '<TH')
                )
#
#
class HtmlReader():
    """
    Clase Lector. Extrae atributos del archivo resultante de la busqueda
     de Protein Prospector. Guarda los atributos en forma lineal y quizas
     deberia ser en forma de arbol
        self.prot_name = ['prot1', 'prot2', 'prot3']
        self.error = ['1','2','2','5','6','7','8','9','3','4','5','6']
    hacer:
        self.prot_name    = ['prot1', 'prot2', 'prot3']
        self.prot_name[1] = {error:[1,2,3], species:['human']} 
    """
    def __init__(self, html, tags, dictags, fname=''):
        self.fname = fname
        self.html = html
        self.tags = tags
        self.prot_name = []
        self.accesion = []
        self.mass = []
        self.error = []
        self.species = []
        self.ppi = []
        self.extract_html()
        self.limpia_startend()
        self.get_match()
        
    def extract_html(self):
        """
        Lee la file de msfit devuelta por el servidor de ProteinProspector
        y guarda los parametros de interes en un diccionario
        
        prot_name [Apolipoprotein A-I precursor (Apo-AI) (ApoA-I), ...]
        accesion  [P15497, Q80Y81, P33471, Q8CGS5]
        mowse     [1.41e+003, 49.4, 38.3, 17.2]
        pmw       [30276.5, 92719.9, 64593.7, 92340.5]
        ppi       [5.71, 7.01, 6.86, 7.03]
        cover     ['23%', '6%', '7%', '4%']
        match     [7/8, 6/8, 5/8, 5/8]
        startend  ['101-106', '177-183', '142-150',...]
        """
        html = self.html
        #
        #print tags
        for key in self.tags:
            indx_ini = 0
            tagitemlist = []
            while True:
                srchtgs = dictags[key]
                start = html.find(srchtgs[0], indx_ini)
                if start == -1: break
                ini = html.find('>', start+1)
                end = html.find(srchtgs[1], ini)
                indx_ini = end
                tagitemlist.append(html[ini+1:end])
            setattr(self, key, tagitemlist)

    def limpia_startend(self):
        """
        divide el texto parseado de startend
        original --> diccio[startend]=[['0.23', '100', '132'],...]
        return   --> diccio[startend]=['100-132', ...]
                 --> diccio[error]= ['0.23, ...']
        """
        selist = []
        erlist = []
        for item in self.startend:
            item = item.replace('</TH>','')
            item = item.replace('<TH>','')
            #print item.split()
            error, start, end = item.split()
            startend = '%03i-%03i' %(int(start), int(end))
            selist.append(startend)
            erlist.append(error)
        self.startend = selist
        self.error = erlist
        
    def get_match(self):
        """
        extrae el numero de matches de
        self.match = ['8/10','6/10','3/10','4/10]
        """
        matches = [item.split(r'/') for item in self.match]
        self.matches = [match for match, total in matches]


if __name__ == "__main__":
    
    import os
    from cStringIO import StringIO
    from xmr_constants import DIRTEST
    import traceback
    
    archivo_prueba = DIRTEST + os.sep + "OB_BSA_G6_b_0001.dat_r.htm"
    html = open(archivo_prueba, 'r').read()
    error_fatal = (KeyboardInterrupt, MemoryError)
    tags = htmltags
               
    try:
        lector = HtmlReader(html, tags, dictags)
        for tag in lector.__dict__:
            if tag == 'file': continue
            for val in getattr(lector, tag):
                print "%s  ---> %s" %(tag, val) 
    except error_fatal:
        raise
    except Exception:
        f = StringIO()
        traceback.print_exc(file=f)
        valor = f.getvalue()
        print valor
#