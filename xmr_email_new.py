#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_email_04
(xmr_14)  29 de abril de 2008
"""
#
#Resultado xmr_email_03
##Date: Thu, 24 Apr 2008 09:48:52 +0200 (CEST) 
##Date-warning: Date header was inserted by damascus.uab.es 
##From: Joaquim.Abian.csic@uab.cat 
##Subject: ENVIO DE UN ROBOT 
##To: Joaquim.Abian.csic@uab.cat 
##
##
##RECIBE EL DOCUMENTO.
##
##
##Content-Type: application/pdf; name=Microsoft Word - OB_2_080228_fac.pdf
##
##
## Microsoft.pdf 
#
#Resultado de este programa xmr_email_04
# Tiene problemas para leer nombre file
# el problema esta en:
# --> subMsg.add_header("Content-type", ctype, name=filename)
##Date: Thu, 24 Apr 2008 10:04:57 +0200 (CEST) 
##Date-warning: Date header was inserted by damascus.uab.es 
##From: Joaquim.Abian.csic@uab.cat 
##Subject: UN ROBOT TE ENVIA UN INFORME 
##To: Joaquim.Abian.csic@uab.cat 
##
##
##RECIBE EL DOCUMENTO.
##
##
##Content-type: application/pdf; name="C:\\datos_xmar\\test\\Microsoft Word -"
##        OB_2_080228_fac.pdf"
##
##
## Cdatos_xmartestMicrosoft Word -.pdf 

import wx
import base64, quopri
import mimetypes
import cStringIO, os
import smtplib
from email import Generator, Message
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from xmr_funcs_11 import aviso

# sample addresses
#toAddr = "mgmbam@iibb.csic.es"
toAddr = "Joaquim.Abian.csic@uab.cat"
fromAddr = "Joaquim.Abian.csic@uab.cat"
outputFile = "dirContentsMail"
archivo_datos = "C:/MP_3_080226_rep.pdf"

def write_message(sender='', to='', subject='', text='', attach=None):
    mainMsg = MIMEMultipart()
    mainMsg["To"] = toAddr
    mainMsg["From"] = fromAddr
    mainMsg["Subject"] = "UN ROBOT TE ENVIA UN INFORME"
    mainMsg.preamble = "Mime message\n"
    mainMsg.epilogue = "\n" # to ensure that message ends with newline
        
    txtMsg = MIMEText(text)
    mainMsg.attach(txtMsg)
    
    for filename in attach:
        ctype, ignored = mimetypes.guess_type(filename)
        if ctype is None:     # If no guess, use generic opaque type
            ctype = "application/octet-stream"
        contentsEncoded = cStringIO.StringIO( )
        f = open(filename, "rb")
        mainType = ctype[:ctype.find("/")]
        if mainType == "text":
            encoding = "quoted-printable"
            quopri.encode(f, contentsEncoded, 1)   # 1 to also encode tabs
        else:
            encoding = "base64"
            base64.encode(f, contentsEncoded)
        f.close( )
        subMsg = Message.Message()
        subMsg.add_header("Content-type", ctype, name=filename)
        subMsg.add_header("Content-transfer-encoding", encoding)
        subMsg.set_payload(contentsEncoded.getvalue( ))
        contentsEncoded.close( )
        mainMsg.attach(subMsg)
     
        return mainMsg.as_string()
#
#
def send_mail(sender, to, message, server):
    ""
    try:
        smtp = smtplib.SMTP(server)
    except socket.gaierror:
        aviso('error al intentar contactar servidor')
        return
    #
    smtp.set_debuglevel(1)
    #
    try:
        error = smtp.sendmail(sender, to, message)
    except:
        aviso('No enviado')
    else:
        if error:
            txt='Could not delivery mail to: '
            for recip in error:
                txt += """%s
                        Server said: %s
                        %s
                        %s\n""" % (recip, smtpresult[recip][0],
                                 smtpresult[recip][1], errstr)
            aviso(txt)
    #
    smtp.quit()
#
#
def mail(sender='', to='', subject='', text='', attach=None, server=""):
    ""
    msg = write_message(sender, to, subject, text, attach)
    send_mail(sender, to, msg, server)


if __name__ == "__main__":
    
    from xmr_constants import DIRTEST
    SMTPSERVER = 'smtp.uab.cat'
    toAddr = "Joaquim.Abian.csic@uab.cat"
    fromAddr = "Joaquim.Abian.csic@uab.cat"
    attach = [DIRTEST + os.sep + r"Microsoft Word - OB_2_080228_fac.pdf"]
    
    app = wx.PySimpleApp()
    
    msg = write_message(sender=fromAddr, to=toAddr,
                        subject='ENVIO DE UN ROBOT',
                        text='RECIBE EL DOCUMENTO.\n\n',
                        attach=attach)
    
    #print msg
    send_mail(fromAddr, toAddr, msg, SMTPSERVER)

					  