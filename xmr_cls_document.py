#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_cls_document_03
(xmr_11) 25 de Febrero de 2008
requiere coregir algunas cosas en el original (nombres mal puestos)
"""
import wx

# begin wxGlade: extracode
# end wxGlade


class BaseDocumentPanel(wx.Panel):
    def __init__(self, *args, **kwds):
        # begin wxGlade: DocumentFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Panel.__init__(self, *args, **kwds)
        self.lb_1 = wx.StaticText(self, -1, "Modo", style=wx.ALIGN_CENTRE)
        self.lb_11 = wx.StaticText(self, -1, "Factura")
        self.chbx_fdoc = wx.CheckBox(self, -1, ".doc")
        self.chbx_fpdf = wx.CheckBox(self, -1, ".pdf")
        self.static_line_1 = wx.StaticLine(self, -1)
        self.chbx_fmail = wx.CheckBox(self, -1, "mail ")
        self.chbx_fsave = wx.CheckBox(self, -1, "save")
        self.chbx_fprint = wx.CheckBox(self, -1, "print")
        self.lb_12 = wx.StaticText(self, -1, "Report")
        self.chbx_rdoc = wx.CheckBox(self, -1, ".doc")
        self.chbx_rpdf = wx.CheckBox(self, -1, ".pdf")
        self.static_line_1_copy = wx.StaticLine(self, -1)
        self.chbx_rmail = wx.CheckBox(self, -1, "mail ")
        self.chbx_rsave = wx.CheckBox(self, -1, "save")
        self.chbx_rprint = wx.CheckBox(self, -1, "print")
        self.lb_13 = wx.StaticText(self, -1, "Proteored")
        self.lb_2 = wx.StaticText(self, -1, "Direcciones", style=wx.ALIGN_CENTRE)
        self.lb_21 = wx.StaticText(self, -1, "mail factura")
        self.tc_mfactura = wx.TextCtrl(self, -1, "")
        self.lb_22 = wx.StaticText(self, -1, "mail report")
        self.tc_mreport = wx.TextCtrl(self, -1, "")
        self.lb_23 = wx.StaticText(self, -1, "mail copy")
        self.tc_mcopy = wx.TextCtrl(self, -1, "")
        self.static_line_2 = wx.StaticLine(self, -1)
        self.lb_241 = wx.StaticText(self, -1, "SERVER")
        self.tc_server = wx.TextCtrl(self, -1, "")
        self.static_line_3 = wx.StaticLine(self, -1, style=wx.LI_VERTICAL)
        self.lb_242 = wx.StaticText(self, -1, "User")
        self.tc_user = wx.TextCtrl(self, -1, "")
        self.lb_251 = wx.StaticText(self, -1, "Pdf Printer")
        self.cbx_printer = wx.ComboBox(self, -1, choices=[], style=wx.CB_DROPDOWN)
        self.static_line_4 = wx.StaticLine(self, -1, style=wx.LI_VERTICAL)
        self.lb_252 = wx.StaticText(self, -1, "Psswrd")
        self.tc_psswrd = wx.TextCtrl(self, -1, "", style=wx.TE_PASSWORD)
        self.lb_3 = wx.StaticText(self, -1, "FORMULARIOS", style=wx.ALIGN_CENTRE)
        self.lb_31 = wx.StaticText(self, -1, "form factura")
        self.tc_ffact = wx.TextCtrl(self, -1, "")
        self.bt_ffact = wx.Button(self, -1, "...")
        self.lb_32 = wx.StaticText(self, -1, "form report")
        self.tc_freport = wx.TextCtrl(self, -1, "")
        self.bt_freport = wx.Button(self, -1, "...")
        self.lb_4 = wx.StaticText(self, -1, "Documentos", style=wx.ALIGN_CENTRE)
        self.chbx_factura = wx.CheckBox(self, -1, "Factura")
        self.chbx_report = wx.CheckBox(self, -1, "Report")
        self.chbx_proteored = wx.CheckBox(self, -1, "Proteored")
        self.bt_make = wx.Button(self, -1, "MAKE")

        self.__set_properties()
        self.__do_layout()
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: DocumentFrame.__set_properties
        #self.SetTitle("Documents Frame")
        self.SetSize((423, 479))
        self.SetBackgroundColour(wx.Colour(255, 237, 191))
        self.lb_1.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.lb_11.SetBackgroundColour(wx.Colour(154, 158, 255))
        self.lb_12.SetBackgroundColour(wx.Colour(154, 158, 255))
        self.lb_13.SetBackgroundColour(wx.Colour(154, 158, 255))
        self.lb_2.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.lb_21.SetMinSize((60, 13))
        self.lb_22.SetMinSize((60, 13))
        self.lb_23.SetMinSize((60, 13))
        self.lb_241.SetMinSize((50, 15))
        self.tc_server.SetMinSize((129, 21))
        self.lb_242.SetMinSize((35, 15))
        self.lb_251.SetMinSize((51, 15))
        self.cbx_printer.SetMinSize((130, 21))
        self.lb_252.SetMinSize((35, 15))
        self.lb_3.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.lb_31.SetMinSize((60, 13))
        self.bt_ffact.SetMinSize((20, 20))
        self.lb_32.SetMinSize((60, 13))
        self.bt_freport.SetMinSize((20, 20))
        self.lb_4.SetBackgroundColour(wx.Colour(255, 255, 0))
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: DocumentFrame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_15 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_14 = wx.BoxSizer(wx.VERTICAL)
        sizer_141 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_12_copy = wx.BoxSizer(wx.VERTICAL)
        sizer_132 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_131 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_12 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_122_copy = wx.BoxSizer(wx.HORIZONTAL)
        sizer_122 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_121 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_11 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_113 = wx.BoxSizer(wx.VERTICAL)
        sizer_112 = wx.BoxSizer(wx.VERTICAL)
        sizer_1121 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_111 = wx.BoxSizer(wx.VERTICAL)
        sizer_1111 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.lb_1, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_111.Add(self.lb_11, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_1111.Add(self.chbx_fdoc, 1, 0, 0)
        sizer_1111.Add(self.chbx_fpdf, 1, wx.LEFT, 3)
        sizer_111.Add(sizer_1111, 0, wx.EXPAND, 0)
        sizer_111.Add(self.static_line_1, 0, wx.ALL|wx.EXPAND, 3)
        sizer_111.Add(self.chbx_fmail, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_111.Add(self.chbx_fsave, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_111.Add(self.chbx_fprint, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_11.Add(sizer_111, 1, wx.ALL|wx.EXPAND, 3)
        sizer_112.Add(self.lb_12, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_1121.Add(self.chbx_rdoc, 1, 0, 0)
        sizer_1121.Add(self.chbx_rpdf, 1, wx.LEFT, 3)
        sizer_112.Add(sizer_1121, 0, wx.EXPAND, 0)
        sizer_112.Add(self.static_line_1_copy, 0, wx.ALL|wx.EXPAND, 3)
        sizer_112.Add(self.chbx_rmail, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_112.Add(self.chbx_rsave, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_112.Add(self.chbx_rprint, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_11.Add(sizer_112, 1, wx.ALL|wx.EXPAND, 4)
        sizer_113.Add(self.lb_13, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_11.Add(sizer_113, 1, wx.ALL|wx.EXPAND, 3)
        sizer_1.Add(sizer_11, 0, wx.EXPAND, 0)
        sizer_12.Add(self.lb_2, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 3)
        sizer_121.Add(self.lb_21, 0, wx.ALL, 2)
        sizer_121.Add(self.tc_mfactura, 1, wx.BOTTOM, 2)
        sizer_12.Add(sizer_121, 0, wx.LEFT|wx.RIGHT|wx.TOP|wx.EXPAND, 3)
        sizer_122.Add(self.lb_22, 0, wx.ALL, 2)
        sizer_122.Add(self.tc_mreport, 1, wx.BOTTOM, 2)
        sizer_12.Add(sizer_122, 0, wx.LEFT|wx.RIGHT|wx.EXPAND, 3)
        sizer_122_copy.Add(self.lb_23, 0, wx.ALL, 2)
        sizer_122_copy.Add(self.tc_mcopy, 1, 0, 0)
        sizer_12.Add(sizer_122_copy, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.EXPAND, 3)
        sizer_12.Add(self.static_line_2, 0, wx.ALL|wx.EXPAND, 3)
        sizer_3.Add(self.lb_241, 0, wx.ALL, 3)
        sizer_3.Add(self.tc_server, 0, wx.LEFT|wx.RIGHT, 5)
        sizer_3.Add(self.static_line_3, 0, wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
        sizer_3.Add(self.lb_242, 0, wx.LEFT|wx.TOP, 2)
        sizer_3.Add(self.tc_user, 0, wx.RIGHT, 4)
        sizer_12.Add(sizer_3, 1, wx.LEFT|wx.RIGHT|wx.TOP|wx.EXPAND, 3)
        sizer_2.Add(self.lb_251, 0, wx.ALL, 2)
        sizer_2.Add(self.cbx_printer, 0, wx.LEFT|wx.RIGHT, 5)
        sizer_2.Add(self.static_line_4, 0, wx.LEFT|wx.RIGHT|wx.EXPAND, 10)
        sizer_2.Add(self.lb_252, 0, wx.LEFT|wx.TOP, 2)
        sizer_2.Add(self.tc_psswrd, 0, wx.RIGHT, 4)
        sizer_12.Add(sizer_2, 0, wx.LEFT|wx.RIGHT|wx.BOTTOM|wx.EXPAND, 3)
        sizer_1.Add(sizer_12, 0, wx.EXPAND, 0)
        sizer_12_copy.Add(self.lb_3, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL, 3)
        sizer_131.Add(self.lb_31, 0, wx.ALL, 2)
        sizer_131.Add(self.tc_ffact, 1, 0, 0)
        sizer_131.Add(self.bt_ffact, 0, 0, 0)
        sizer_12_copy.Add(sizer_131, 1, wx.ALL|wx.EXPAND, 3)
        sizer_132.Add(self.lb_32, 0, wx.ALL, 2)
        sizer_132.Add(self.tc_freport, 1, wx.EXPAND, 0)
        sizer_132.Add(self.bt_freport, 0, 0, 0)
        sizer_12_copy.Add(sizer_132, 1, wx.ALL|wx.EXPAND, 3)
        sizer_1.Add(sizer_12_copy, 0, wx.EXPAND, 0)
        sizer_14.Add(self.lb_4, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 3)
        sizer_141.Add((20, 20), 1, 0, 0)
        sizer_141.Add(self.chbx_factura, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5)
        sizer_141.Add(self.chbx_report, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5)
        sizer_141.Add(self.chbx_proteored, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5)
        sizer_141.Add((20, 20), 1, 0, 0)
        sizer_14.Add(sizer_141, 0, wx.EXPAND, 3)
        sizer_1.Add(sizer_14, 1, wx.EXPAND, 0)
        sizer_15.Add((20, 20), 1, 0, 0)
        sizer_15.Add(self.bt_make, 0, 0, 0)
        sizer_15.Add((20, 20), 1, 0, 0)
        sizer_1.Add(sizer_15, 0, wx.ALL|wx.EXPAND, 3)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade
        # end wxGlade
        # end wxGlade

# end of class DocumentFrame


class DocumentFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        wx.Frame.__init__(self, *args, **kwds)
        self.SetTitle("Documents")
        self.SetSize((420, 530))
        self.pan = BaseDocumentPanel(self, -1)
    

# end of class DocumentFrame
class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        documentframe = DocumentFrame(None, -1, "")
        self.SetTopWindow(documentframe)
        documentframe.Show()
        return 1

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
