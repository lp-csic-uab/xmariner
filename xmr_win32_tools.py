#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
"""
xmr_win32_tools_03
(xmr_15) 28 de abril de 2008
"""
#
import os
import time
from pxword import Word
#
#
#
class WordDoc(Word):
    ""
    def __init__(self, doc):
        Word.__init__(self, doc)
    
    def substituye_marcas(self, codigos):
        #time.sleep(2)
        for mark, dato in codigos:
            self.chRemplAll(mark, dato)
        #time.sleep(2)
            
    def printa(self, printer='', pdf=False):
        ""
        if pdf:
            self.changePrinter(printer)
            #self.changePrinter("Acrobat PDFWriter")
        self.wprint()

    def inserta_doc(self, pagepos='last', documento=''):
        ""
        if pagepos == 'last': return 
        
        self.goto_page(pagepos)
        self.insert_file(documento)
    
    def haz_magia(self):
        ""
        doc = self.w.ActiveDocument
        sel = self.w.Selection
        tablas = doc.Tables.Count
        #
        for idx in range(tablas):
            self.goto_table(idx+1)
            sel.Tables(1).Select()
            #time.sleep(4)
            sel.Start = sel.Start - 1
            if idx == 0:
                doc.Range(sel.Start, sel.Start).InsertBreak(Type=2)
            sel.Start = sel.Start + 1
            #time.sleep(2)
            doc.Range(sel.End, sel.End).InsertBreak(Type=2)
            #time.sleep(2)
            sel.PageSetup.Orientation=1

            for i in range(1,8):
                self.select_columns(0, i+1)
                self.textsize(9)
            
##            self.w.Selection.Tables(1).Select()
##            self.w.Selection.Tables(1).AutoFitBehavior(1)
        
        self.goto_page(2)
        sel.TypeBackspace()   #Para eliminar la pagina vacia que se ha
                                            #generado con los breaks ...    

def insert_data(nombre_documento, form, salvarreport):
    report = WordDoc(form)
    report.inserta_doc(2, nombre_documento)
    report.haz_magia()
    #
    if salvarreport:
        report.saveas(nombre_documento + "_rep.doc")
        report.printa(pdf=True)
        time.sleep(3)


if __name__ == "__main__":
    
    from xmr_constants import DIRTEST, FORM_REPORT
    
    #archivo = "OB_2.htm"
    archivo = "MH_3.htm"
    
    filename = DIRTEST + os.sep + archivo
    insert_data(filename, FORM_REPORT, 1)
