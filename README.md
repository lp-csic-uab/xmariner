
---

**WARNING!**: This is the *Old* source-code repository for xMariner program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/xmariner/) located at https://sourceforge.net/p/lp-csic-uab/xmariner/**  

---  
  
  
# xMariner program

xMariner automates the analysis of MALDI spectra and report generation.

---

**WARNING!**: This is the *Old* source-code repository for xMariner program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/xmariner/) located at https://sourceforge.net/p/lp-csic-uab/xmariner/**  

---  
  

