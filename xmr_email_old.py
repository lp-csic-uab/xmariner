#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_email_02
(xmr_13)  10 de marzo de 2008
"""
#
import wx
import smtplib, socket
import MimeWriter, mimetypes, mimetools
import os
import time
import StringIO
from xmr_funcs_11 import aviso

AUTHREQUIRED = 0        # if you need to use SMTP AUTH set to 1
smtpuser = '1187714@pop.uab.cat'  # for SMTP AUTH, set SMTP username here
smtppass = 'paris2007'  # for SMTP AUTH, set SMTP password here
#
#
def write_message(sender='', to='', subject='', text='', attach=None):
    """
    Usage:
        mail()
    Params:
        sender: sender's email address
        to: receipient email address
        text: Email message body main part.
        attachments: list of files to attach
    """
    message = StringIO.StringIO()
    writer = MimeWriter.MimeWriter(message)
    writer.addheader('To', to)
    writer.addheader('From', sender)
    writer.addheader('Subject', subject)
    writer.addheader('MIME-Version', '1.0')
    
    writer.startmultipartbody('mixed')
    
    # start with a text/plain part
    part = writer.nextpart()
    body = part.startbody('text/plain')
    part.flushheaders()
    body.write(text)

    # now add the attachments
    if attach is not None:
        for a in attach:
            filename = os.path.basename(a)
            print 'attach ', a
            print 'filename ', filename
            ctype, encoding = mimetypes.guess_type(a)
            if ctype is None:
                ctype = 'application/octet-stream'
                encoding = 'base64'
            elif ctype == 'text/plain':
                encoding = 'quoted-printable'
            else:
                encoding = 'base64'
                
            part = writer.nextpart()
            part.addheader('Content-Transfer-Encoding', encoding)
            body = part.startbody("%s; name=%s" % (ctype, filename))
            n = 0
            while n < 5:
                try:
                    mimetools.encode(open(a, 'rb'), body, encoding)
                    break
                except IOError:
                    n += 1
                    print n
                    time.sleep(2)
    #
    writer.lastpart()
    return message
#
#
def send_mail(sender, to, message, server):
    ""
    try:
        smtp = smtplib.SMTP(server)
    except socket.gaierror:
        aviso('error al intentar contactar servidor')
        return
    #
    smtp.set_debuglevel(1)
    #
    try:
        error = smtp.sendmail(sender, to, message.getvalue())
    except:
        aviso('No enviado')
    else:
        if error:
            txt='Could not delivery mail to: '
            for recip in error:
                txt += """%s
                        Server said: %s
                        %s
                        %s\n""" % (recip, smtpresult[recip][0],
                                 smtpresult[recip][1], errstr)
            aviso(txt)
    #
    smtp.quit()
#
#
def mail(sender='', to='', subject='', text='', attach=None, server=""):
    ""
    msg = write_message(sender, to, subject, text, attach)
    send_mail(sender, to, msg, server)


if __name__ == "__main__":
    
    from xmr_constants import DIRTEST
    SMTPSERVER = 'smtp.uab.cat'
    toAddr = "Joaquim.Abian.csic@uab.cat"
    fromAddr = "Joaquim.Abian.csic@uab.cat"
    attach = [DIRTEST + os.sep + "Microsoft Word - OB_2_080228_fac.pdf"]
    
    app = wx.PySimpleApp()
   
    msg = write_message(sender=fromAddr, to=toAddr,
                        subject='ENVIO DE UN ROBOT',
                        text='RECIBE EL DOCUMENTO.\n\n',
                        attach=attach)
    
    msg.seek(0)
    print msg.read()
    
#    send_mail(fromAddr, toAddr, msg, SMTPSERVER)