# -*- coding: cp1252 -*-

#
# by Michel Claveau Informatique
# Modificado por JAbian2008
# http://mclaveau.com
#

import time
from win32com.client import Dispatch
import pywintypes

GlobalPVersion='1.5'


class Word(object):

    wdReplaceNone=0
    wdReplaceOne=1
    wdReplaceAll=2
    wdFindContinue=1

    #pour close/save :
    wdDoNotSaveChanges=0
    wdSaveChanges=-1

    wdCharacter=1
    wdCell=12
    wdLine=5
    
    wdAlignLeft=0
    wdAlignCenter=1
    wdAlignRight=2

    def __init__(self, arch=None, visible=True):
        import win32com.client
        import time
        self.w = win32com.client.Dispatch('Word.Application')
        if arch != None:
            time.sleep(2)
            self.open(arch, visible)

    def u1252(self, chu):
        try: 
            if type(chu) is unicode:
                return chu.encode('cp1252','replace')
            else:
                return chu
        except:
            return repr(chu)

    def open(self, arch='', visible=True):
        """Open a word document
        """
        if not arch:
            self.doc = self.w.Documents.Add()
        else:            
            self.doc = self.w.Documents.Open(arch)
        self.visible(True)

    def insert_file(self, arch):
        "Inserta un archivo en el documento)"
        self.w.Selection.InsertFile(FileName=arch)
    
    def close(self):
        """Close le document, en sauvegardant, sans demander
        """
        self.w.ActiveDocument.Close(SaveChanges = self.wdDoNotSaveChanges)

    def saveas(self, arch, typ=0):
        """Appel de 'Enregistrer sous', avec le nom du arch

           wdFormatDocument=0		# format Word. Valeur par d�faut. 
           wdFormatTemplate=1		# mod�le Word
           wdFormatText=2		# texte sans mise en forme (ANSI)
           wdFormatTextLineBreaks=3	# texte sans mise en forme
           wdFormatDOSText=4		# texte sans mise en forme (ANSI) 
           wdFormatDOSTextLineBreaks=5	# texte sans mise en forme, mais avec sauts de ligne.
           wdFormatRTF=6		# RTF
           wdFormatUnicodeText=7	# texte_Unicode/texte_Encod�. Param�tre suppl�mentaire pour l'encodage.
           wdFormatHTML=8		# HTML
        """
        self.doc.SaveAs(arch, typ)

    def quit(self):
        """Close word
        """
        self.w.Quit()

    def quitsave(self):
        """Close word, en sauvant les changements
        """
        self.w.Quit(self.wdSaveChanges)

    def quitcancel(self):
        """Close word, SANS sauver les changements
        """
        self.w.Quit(self.wdDoNotSaveChanges)

    def visible(self, par=True):
        """Rend Word visible (True), ou invisible (False) ; True par d�faut
        Note : c'est plus rapide en invisible
        """
        if par:
            self.w.Visible=True
        else:
            self.w.Visible=False

    def hide(self):
        """Cache Word
        """
        self.w.visible=False

    def wprint(self):
        """Imprime el documento
        Curiosamente PrintOut no acepta keyword FileName... aunque sale en macros
        """
        try:
            self.doc.PrintOut()
        except pywintypes.com_error:
            print 'error de printado'
            
    def preview(self):
        """Pr�-visualise le document
        """
        self.doc.PrintPreview()

    def previewclose(self):
        """Close la pr�visdualisation du document
        """
        self.doc.ClosePrintPreview()

    def text(self, txt):
        """Remplace le texte s�lectionn�, par le param�tre
        """
        newtexto=txt.replace('\n','\r')
        self.position.Text = newtexto

    def TypeText(self, texto):
        """ 'Tape' le texte � la position courante
        """
        self.position.TypeText(texto)

    def chSelect(self, chtexto):
        """S�lectionne (recherche) la texto pass�e en param�tre
        """
        sel = self.w.Selection
        #sel.ClearFormatting()
        sel.Find.Text = chtexto
        sel.Find.Forward = True
        sel.Find.Execute()
        self.position=sel
        sel.Select()

    def macroRun(self, name):
        """Lance la macro-word (VBA) 'name'
        """
        self.w.Run(name)

    def language(self):
        """Retourne la langue de Word, ainsi que le code-langue de word
        """
        id=self.w.LanguageSettings.LanguageID(2)
        if id==1078	:  l="Afrikaans"
        elif id==1052	:  l="Albanian"
        elif id==14337	:  l="Arabic - United Arab Emirates"
        elif id==15361	:  l="Arabic - Bahrain"
        elif id==5121	:  l="Arabic - Algeria"
        elif id==3073	:  l="Arabic - Egypt"
        elif id==2049	:  l="Arabic - Iraq"
        elif id==11265	:  l="Arabic - Jordan"
        elif id==13313	:  l="Arabic - Kuwait"
        elif id==12289	:  l="Arabic - Lebanon"
        elif id==4097	:  l="Arabic - Libya"
        elif id==6145	:  l="Arabic - Morocco"
        elif id==8193	:  l="Arabic - Oman"
        elif id==16385	:  l="Arabic - Qatar"
        elif id==1025	:  l="Arabic - Saudi Arabia"
        elif id==10241	:  l="Arabic - Syria"
        elif id==7169	:  l="Arabic - Tunisia"
        elif id==9217	:  l="Arabic - Yemen"
        elif id==1067	:  l="Armenian"
        elif id==1068	:  l="Azeri - Latin"
        elif id==2092	:  l="Azeri - Cyrillic"
        elif id==1069	:  l="Basque"
        elif id==1059	:  l="Belarusian"
        elif id==1026	:  l="Bulgarian"
        elif id==1027	:  l="Catalan"
        elif id==2052	:  l="Chinese - China"
        elif id==3076	:  l="Chinese - Hong Kong SAR"
        elif id==5124	:  l="Chinese - Macau SAR"
        elif id==4100	:  l="Chinese - Singapore"
        elif id==1028	:  l="Chinese - Taiwan"
        elif id==1050	:  l="Croatian"
        elif id==1029	:  l="Czech"
        elif id==1030	:  l="Danish"
        elif id==1043	:  l="Dutch - The Netherlands"
        elif id==2067	:  l="Dutch - Belgium"
        elif id==3081	:  l="English - Australia"
        elif id==10249	:  l="English - Belize"
        elif id==4105	:  l="English - Canada"
        elif id==9225	:  l="English - Caribbean"
        elif id==6153	:  l="English - Ireland"
        elif id==8201	:  l="English - Jamaica"
        elif id==5129	:  l="English - New Zealand"
        elif id==13321	:  l="English - Phillippines"
        elif id==7177	:  l="English - South Africa"
        elif id==11273	:  l="English - Trinidad"
        elif id==2057	:  l="English - United Kingdom"
        elif id==1033	:  l="English - United States"
        elif id==1061	:  l="Estonian"
        elif id==1065	:  l="Farsi"
        elif id==1035	:  l="Finnish"
        elif id==1080	:  l="Faroese"
        elif id==1036	:  l="French - France"
        elif id==2060	:  l="French - Belgium"
        elif id==3084	:  l="French - Canada"
        elif id==5132	:  l="French - Luxembourg"
        elif id==4108	:  l="French - Switzerland"
        elif id==2108	:  l="Gaelic - Ireland"
        elif id==1084	:  l="Gaelic - Scotland"
        elif id==1031	:  l="German - Germany"
        elif id==3079	:  l="German - Austria"
        elif id==5127	:  l="German - Liechtenstein"
        elif id==4103	:  l="German - Luxembourg"
        elif id==2055	:  l="German - Switzerland"
        elif id==1032	:  l="Greek"
        elif id==1037	:  l="Hebrew"
        elif id==1081	:  l="Hindi"
        elif id==1038	:  l="Hungarian"
        elif id==1039	:  l="Icelandic"
        elif id==1057	:  l="Indonesian"
        elif id==1040	:  l="Italian - Italy"
        elif id==2064	:  l="Italian - Switzerland"
        elif id==1041	:  l="Japanese"
        elif id==1042	:  l="Korean"
        elif id==1062	:  l="Latvian"
        elif id==1063	:  l="Lithuanian"
        elif id==1071	:  l="FYRO Macedonian"
        elif id==1086	:  l="Malay - Malaysia"
        elif id==2110	:  l="Malay � Brunei"
        elif id==1082	:  l="Maltese"
        elif id==1102	:  l="Marathi"
        elif id==1044	:  l="Norwegian - Bokm�l"
        elif id==2068	:  l="Norwegian - Nynorsk"
        elif id==1045	:  l="Polish"
        elif id==2070	:  l="Portuguese - Portugal"
        elif id==1046	:  l="Portuguese - Brazil"
        elif id==1047	:  l="Raeto-Romance"
        elif id==1048	:  l="Romanian - Romania"
        elif id==2072	:  l="Romanian - Moldova"
        elif id==1049	:  l="Russian"
        elif id==2073	:  l="Russian - Moldova"
        elif id==1103	:  l="Sanskrit"
        elif id==3098	:  l="Serbian - Cyrillic"
        elif id==2074	:  l="Serbian - Latin"
        elif id==1074	:  l="Setsuana"
        elif id==1060	:  l="Slovenian"
        elif id==1051	:  l="Slovak"
        elif id==1070	:  l="Sorbian"
        elif id==1034	:  l="Spanish - Spain"
        elif id==11274	:  l="Spanish - Argentina"
        elif id==16394	:  l="Spanish - Bolivia"
        elif id==13322	:  l="Spanish - Chile"
        elif id==9226	:  l="Spanish - Colombia"
        elif id==5130	:  l="Spanish - Costa Rica"
        elif id==7178	:  l="Spanish - Dominican Republic"
        elif id==12298	:  l="Spanish - Ecuador"
        elif id==4106	:  l="Spanish - Guatemala"
        elif id==18442	:  l="Spanish - Honduras"
        elif id==2058	:  l="Spanish - Mexico"
        elif id==19466	:  l="Spanish - Nicaragua"
        elif id==6154	:  l="Spanish - Panama"
        elif id==10250	:  l="Spanish - Peru"
        elif id==20490	:  l="Spanish - Puerto Rico"
        elif id==15370	:  l="Spanish - Paraguay"
        elif id==17418	:  l="Spanish - El Salvador"
        elif id==14346	:  l="Spanish - Uruguay"
        elif id==8202	:  l="Spanish - Venezuela"
        elif id==1072	:  l="Sutu"
        elif id==1089	:  l="Swahili"
        elif id==1053	:  l="Swedish - Sweden"
        elif id==2077	:  l="Swedish - Finland"
        elif id==1097	:  l="Tamil"
        elif id==1092	:  l="Tatar"
        elif id==1054	:  l="Thai"
        elif id==1055	:  l="Turkish"
        elif id==1073	:  l="Tsonga"
        elif id==1058	:  l="Ukrainian"
        elif id==1056	:  l="Urdu"
        elif id==2115	:  l="Uzbek - Cyrillic"
        elif id==1091	:  l="Uzbek � Latin"
        elif id==1066	:  l="Vietnamese"
        elif id==1076	:  l="Xhosa"
        elif id==1085	:  l="Yiddish"
        elif id==1077	:  l="Zulu"
        return l

    def filterTxt(self):
        """Convertit une s�lection en texte
        """
        ss=self.u1252(self.w.Selection.Text)
        ss=ss.replace(chr(7)+chr(13),'   ')
        ss=ss.replace(chr(13),'\r\n')
        ss=ss.replace(chr(7),' ')
        ss=ss.replace(chr(9),'')
        ss=ss.replace(chr(26),'')
        return ss

    def textsize(self, size):
        "cambia el tama�o de texto de una seleccion"
        self.w.Selection.Font.Size = size
    
    def goto_page(self, np):
        np = str(np)
        self.w.Selection.GoTo(1, 1, '2')
        
    def goto_table(self, nt):
        self.w.Selection.GoTo(2, 1, nt)  #??
    
    def eSelAll(self):
        """s�lectionne, et retourne, tout le document
        """
        sel=self.w.Selection.WholeStory()
        return self.filterTxt()

    def eSelWord(self, nb=1):
        """�tend la s�lection aux nb mots � droite, et retourne la s�lection
        """
        self.w.Selection.MoveRight(self.wdWord, nb, self.wdExtend)
        return self.filterTxt()

    def eSelLine(self, nb=1):
        """�tend la s�lection aux nb lignes en-dessous, et retourne la s�lection
        """
        self.w.Selection.MoveDown(self.wdLine, nb, self.wdExtend)
        return self.filterTxt()

    def eSelEndLine(self):
        """�tend la s�lection jusqu'� la fin de la ligne, et retourne la s�lection
        """
        self.w.Selection.EndKey(self.wdLine, self.wdExtend)
        return self.filterTxt()

    def chRemplAll(self, oldtexto, newtexto=''):
        """
        oldtexto = texto a remplacer / string to replace
        newtexto = texto de remplacement / string for replace
        """
        sel = self.w.Selection
        #sel.ClearFormatting()
        sel.Find.Text = oldtexto
        sel.Find.Forward = True
        newtexto=newtexto.replace('\n','\r')
        sel.Find.Execute( oldtexto,False,False,False,False,False,True,self.wdFindContinue,False,newtexto,self.wdReplaceAll)
        self.position=sel

    def chRemplOne(self, oldtexto, newtexto=''):
        """
        oldtexto = texto a remplacer / string to replace
        newtexto = texto de remplacement / string for replace
        """
        sel = self.w.Selection
        #sel.ClearFormatting()
        sel.Find.Text = oldtexto
        sel.Find.Forward = True
        newtexto=newtexto.replace('\n','\r')
        sel.Find.Execute( oldtexto,False,False,False,False,False,True,self.wdFindContinue,False,newtexto,self.wdReplaceOne)
        self.position=sel

    def chRemplClipboard(self, oldtexto):
        """
        oldtexto = texto a remplacer / string to replace
        """
        sel = self.w.Selection
        #sel.ClearFormatting()
        sel.Find.Text = oldtexto
        sel.Find.Forward = True
    
        sel.Find.Execute( oldtexto,False,False,False,False,False,True,self.wdFindContinue,False,'XXX',self.wdReplaceOne)
        sel.Paste()
        self.position=sel

    def chRemplGraf(self, oldtexto, arch):
        """
        oldtexto = texto a remplacer / string to replace
        """
        sel = self.w.Selection
        #sel.ClearFormatting()
        sel.Find.Text = oldtexto
        sel.Find.Forward = True
    
        sel.Find.Execute( oldtexto,False,False,False,False,False,True,self.wdFindContinue,False,'',self.wdReplaceOne)
        sel.InlineShapes.AddPicture(arch, False, True)
        self.position=sel

    def select_columns(self, tabla, col=0):
        "selecciona columna (col) de tabla (int)"
        
        tb = self.doc.Tables[tabla]
        if col == 0:
            tb.Columns.Select()
        else:
            tb.Columns(col).Select()
    
    def TablaInsLigApres(self, oldtexto, nblig=1):
        """
        oldtexto = texto a remplacer / string to replace
        """
        sel = self.w.Selection
        #sel.ClearFormatting()
        sel.Find.Text = oldtexto
        sel.Find.Forward = True
    
        sel.Find.Execute( oldtexto,False,False,False,False,False,True,self.wdFindContinue,False,'',self.wdReplaceOne)
        sel.InsertRowsBelow(nblig)

    def TablaDelLig(self, oldtexto):
        """
        oldtexto = texto a remplacer / string to replace
        """
        sel = self.w.Selection
        #sel.ClearFormatting()
        sel.Find.Text = oldtexto
        sel.Find.Forward = True
    
        sel.Find.Execute( oldtexto,False,False,False,False,False,True,self.wdFindContinue,False,'',self.wdReplaceOne)
        sel.Rows.Delete()

    def MoveRight(self, nb=1):
        self.position.MoveRight(self.wdCharacter, nb)

    def MoveLeft(self, nb=1):
        self.position.MoveLeft(self.wdCharacter, nb)

    def TablaMoveRight(self, nb=1):
        sel = self.w.Selection
        sel.MoveRight(self.wdCell, nb)

    def TablaMoveLeft(self, nb=1):
        sel = self.w.Selection
        sel.MoveLeft(self.wdCell, nb)

    def TablaMoveLine(self, nb=1):
        sel = self.w.Selection
        if nb>0:
            sel.MoveDown(self.wdLine, nb)
        else:
            sel.MoveUp(self.wdLine, -nb)

    def TablaCelda(self, lig=1, col=1, txt='', align=0):
        tbl = self.doc.Tables[0]
        Celda = tbl.Cell(lig, col)
        Celda.Range.Text = txt 
        Celda.Range.ParagraphFormat.Alignment = align  #0,1,2, left, center, right

    def landscape(self):
        """Met le document en mode paysage
        """
        self.wdOrientLandscape=1
        self.wdOrientPortrait=0
        self.w.ActiveDocument.PageSetup.Orientation = self.wdOrientLandscape 

    def portrait(self):
        """Met le document en mode portrait
        """
        self.wdOrientLandscape=1
        self.wdOrientPortrait=0
        self.w.ActiveDocument.PageSetup.Orientation = self.wdOrientPortrait 

    def changePrinter(self, printerName):
        """Change l'imprimante active de Word
        """
        self.w.ActivePrinter = printerName


if __name__=='__main__':

    pass
    """
    w=Word()
    w.open("C:\\LET01.doc")

    w.chRemplAll('.NOM.','Tartempion')

    w.TablaCelda(1,1,"1111",0)
    w.TablaCelda(1,2,"1122",1)
    w.TablaCelda(2,2,"2222",2)

    w.chSelect('ORP')
    w.TypeText("TTTT�TTTT")
    w.quit()
    """

#********************************************
    #w=word('C:\\LET01.doc',False)
    
#    """  
    w=Word()
    w.open("C:\\LET01.doc")
    time.sleep(5)

    #w.visible(False)
    w.visible(True)
    #w.visible()
    #w.hide()

    w.chRemplAll('.NOM.','Antonio Jimenez')
    #time.sleep(2)

    #w.chRemplClipboard('.ADR1.')
    
    w.chRemplOne('.ADR1.','Instituto de Neurociencias')
    w.chRemplOne('.ADR2.','Jordi Girona, 26')
    time.sleep(2)
    w.chRemplAll('.CPOSTAL.','07700')
    w.chRemplAll('.CITY1.','Barcelona')
    w.chRemplAll('.CITY2.','Bellaterra')
    w.chRemplAll('.CEJOUR.','25 febrero 2008')
    time.sleep(2)

    w.chRemplOne('.REFERENCE.','#12345')
    w.MoveLeft()
    w.MoveRight(6)
    w.text('11')
    w.MoveLeft(4)
    w.text('22')

    w.chRemplOne('.BODY.','''Hola, como os va?, Os enviamos la 
foto y los datos.''')

    w.chRemplOne('.TAB2.','.TAB2.')
    w.TablaInsLigApres('.TAB2.', 2)
    w.TablaDelLig('.TAB2.')

    w.chRemplAll('.TAB11.','Muestra')
    w.chRemplAll('.TAB12.','Hit')
    w.chRemplAll('.TAB13.','Score')
    w.chRemplAll('.TAB31.','OB_01_AC')
    w.chRemplAll('.TAB32.','Albumin')
    w.chRemplAll('.TAB33.','0.25')

##    w.TablaMoveRight(3)
##    time.sleep(2)
##    w.TablaMoveLine(1)
##    w.text('AAAAA')
##    w.TablaMoveLeft(2)
##    w.TablaMoveLine(-1)
##    w.text("AZERTY")
##    time.sleep(2)

    #w.chRemplGraf('.GRAF1.',r'C:\rateau_red.jpg')

    time.sleep(3)
    w.changePrinter("Acrobat PDFWriter")
    w.wprint()

    #w.preview()
    #time.sleep(5)
    #w.previewclose()
    #time.sleep(1)

    w.saveas('c:\\Toto.rtf',6)

    w.close()
    w.quit()
"""
#***************************
    w=word()
    w.open("C:\\FORM.doc")
    time.sleep(5)

    #w.visible(False)
    w.visible(True)
    #w.visible()
    #w.hide()

    w.chRemplAll('.NOM.','Carme Quero')
    time.sleep(2)

    #w.chRemplClipboard('.ADR1.')
    
    
    w.chRemplOne('.LAB.','Dpto. Qu�mica Org�nica')
    w.chRemplOne('.ADD1.','Centro de Investigaci�n y Desarrollo (CID)')
    w.chRemplOne('.ADD2.','C/ Jordi Girona, 18-26')
    time.sleep(2)
    w.chRemplAll('.TRAT.','Dra')
    w.chRemplAll('.CP.','08034')
    w.chRemplAll('.CITY.','Barcelona')
    w.chRemplAll('.CITY2.','Bellaterra')
    w.chRemplAll('.NIF.','Q2818002D')
    time.sleep(2)

    w.chRemplOne('.FEM.','Mayo 2007')
    w.chRemplOne('.FF.','Agosto 2007')
    w.chRemplOne('.REF.','113/2007')
    
    w.chRemplOne('.ANALYSIS.', "Peptide Mass Fingerprinting by MS-MALDI TOF")

    total = 13
    mapeo =  5
    resto = total - mapeo
    coste = 5 * 28 + resto * 25.2
    
    w.chRemplOne('.TOTM.', str(total))
    w.chRemplAll('.MM.', str(mapeo))
    w.chRemplAll('.MS.', str(resto))
    w.chRemplAll('.IMPORT.', str(coste))
    
    time.sleep(3)
    w.changePrinter("Acrobat PDFWriter")
    w.wprint()
    
    w.preview()
    time.sleep(5)
    w.previewclose()
    time.sleep(1)

    w.saveas('c:\\Toto.rtf', 6)

    w.close()
    w.quit()
    
   """

