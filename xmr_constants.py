#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_constants
(xmr_15)28 de abril de 2008
"""
#
import os
##
##
##directorios
DIRROOT = r"C:"
DIRQUIM = DIRROOT + os.sep + r"datos_xmar"
DIRRESULTS = DIRQUIM + os.sep + 'results'
DIRTEST = DIRQUIM + os.sep + 'test'
DIRMEMO = DIRQUIM
##
##
##Forms
DIRFORMS = DIRQUIM + os.sep + 'forms'
##
FORM_REPORT = DIRFORMS + os.sep + 'REPORT_FORM_1.doc'
FORM_FACTURAS = DIRFORMS + os.sep + 'FACT_FORM_1.doc'
##
##
##Listas
ARCH_TIPANAL = "lista_tipanals.xls"
ARCH_USER = "lista_users.xls"
ARCH_ANAL = "lista_anals.xls"
##
##
##PDF
DIRPDF = 'C:\\Archivos de programa\\Adobe\\Acrobat 4.0\\PDF Output'
PRINTER_CHOICES = ['Acrobat Distiller',
                   'Acrobat PDFWriter'
                    ]
##
##
##SERVER
SMTPSERVER = 'smtp.uab.cat'
SERVER_USER = ''
SERVER_PASSWORD = ''
##
##
##PROSPECTOR SERVER
PPSERVER = { 'local':"http://158.109.210.59/prospector/ucsfbin3.2/msfit.cgi?",
             'UCSF':None
            }
##
##
##MAIL
SENDER = 'QUIM laboratorio'
MFACT = 'lp.csic@uab.cat'
MCOPY = ''
TEXTFACTURA = 'Estimada amiga:\nAqui tienes esta factura'
TEXTREPORT = 'Estimados amigos:\nAqui teneis esta factura'
##
##
##PROTEORED
ARCH_PROTEORED = DIRQUIM + os.sep + 'proteored.xls'
