# exWx/setup.py
from distutils.core import setup
import py2exe

setup(
    windows=[
            {'script': "xmr_main_14.pyw",
            'icon_resources':[(0,'xmr48.ico')]
            }
            ],
            
    options={
            'py2exe': {
                        #'packages' :    [],
                        'includes':     ['email'],
                        'excludes':     [
                                         'Tkconstants','Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
            },
    zipfile = None,
    data_files= [
                ]
    )
