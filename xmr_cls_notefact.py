#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_cls_notefact_11
(xmr_15) 28 de Abril de 2008
"""
import wx
import time
import os
import base64
import cStringIO
from xmr_constants import FORM_REPORT, FORM_FACTURAS, DIRQUIM, DIRPDF
from xmr_constants import ARCH_TIPANAL, PRINTER_CHOICES, SMTPSERVER, MFACT
from xmr_constants import TEXTFACTURA, TEXTREPORT, SENDER
from xmr_constants import DIRMEMO, MCOPY, ARCH_PROTEORED, ARCH_USER, ARCH_ANAL
from xmr_funcs_11 import get_filepath, aviso, dame_ok
from xmr_funcs_11 import calcula_coste, genera_txt_costes
from xmr_cls_escriba_03 import Escriba, codes_user, codes_anal
from xmr_cls_user_03 import BaseUserPanel
from xmr_cls_anal_05 import BaseAnalPanel
from xmr_cls_document_04 import BaseDocumentPanel
from xmr_win32_tools_03 import WordDoc
from xmr_email_04 import mail
from xmr_images import ICON
#
#
#
def set_globals():
    "genera valores globales que sobregraban los importados de xmr_constants"
    for linea in open('xmr_config_docs', 'r'):
        if linea.startswith('#'): continue
        key, value = linea.strip().split('=')
        if ';;' in value: value = value.split(';;')  #para listas
        globals()[key]=value
#
#
class UserPanel(BaseUserPanel):
    def __init__(self, parent, memosite, *args, **kwds):
        BaseUserPanel.__init__(self, parent, *args, **kwds)
        self.user_dict = {}
        memofile = memosite + os.sep + ARCH_USER
        self.scriba = Escriba('user', memofile)
        self.user_dict = self.scriba.lee_datos()
        self.userchoices = [self.user_dict[user]['user'] for user in self.user_dict]
        self.cbx_user.SetItems(self.userchoices)
        
        self.Bind(wx.EVT_TEXT, self.on_mod)
        self.Bind(wx.EVT_COMBOBOX, self.fill_userdata, self.cbx_user)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.bt_save)
        self.Bind(wx.EVT_BUTTON, self.on_new, self.bt_new)
        
        self.set_tooltips()
        self.bt_save.Enable(False)
        
    def on_mod(self, evt):
        self.bt_save.Enable(True)
    
    def set_tooltips(self):
        ""
        SAVE = 'Guarda datos en lista_users.xls'
        QUIT = 'Salir'
        NEW = 'Borrar todo. Nuevo Usuario'
        #
        tipdic = dict( bt_save = SAVE,
                       bt_quit = QUIT,
                       bt_new = NEW
                      )
        #
        for key in tipdic:
            widget = getattr(self, key)
            widget.SetToolTipString(tipdic[key])
        
    def fill_userdata(self, evt):
        """extrae el nombre de los widgets de esta clase que contienen
        cbx_ (comboboxes) o tc_ (text_Crtl).
        La clase se ha nombrado ex-profeso utilizando estos codigos
        la key del dicionario de usuarios corresponden con estos atributos
        de forma que puede trabajarse automaticamente
        atributos de la clase -> ['tc_fcdd', 'tc_fdfscl', ... , 'tc_uap2']
        keys del dicionario -> ['fcdd', 'fdfscl', ... , 'uap2']
        """
        user = self.cbx_user.GetStringSelection()
        self.fill_data_of_user(user)
        
    def fill_data_of_user(self, user):
        ""
        atributes = [atribute for atribute in self.__dict__ if 
                    (atribute.startswith('cbx_') or atribute.startswith('tc_'))]
        
        try:
            diccionario = self.user_dict[user]
        except KeyError:
            self.check_why_not_user(user)
            return
            
        for key in diccionario:
            a = 'tc_' + key
            b = 'cbx_' + key
            if a in atributes:
                unbf = getattr(self, a)
                txt = diccionario[key]
                lista = txt.split('|')
                unbf.Clear()
                for item in lista[:-1]:
                    unbf.AppendText(item)
                    unbf.AppendText('\n')
                unbf.AppendText(lista[-1])
                continue
            if b in atributes:
                unbf = getattr(self, b)
                unbf.SetValue(diccionario[key])
                
            
    def check_why_not_user(self, user):
        ""
        if 1 < len(user) < 4:
            aviso('el usuario %s es nuevo. Debe crear nueva cuenta' %user)
        else:
            aviso('El codigo de usuario %s puede ser incorrecto.\nSolo debe tener 2 a 3 caracteres' %user)
        
        
    def on_save(self, evt):
        "salva el que esta visible...o todos"
        diccionario = {}
        dicdatos = {}
        
        user = self.cbx_user.GetValue()
        if not user:
            aviso('no hay usuario')
            return
        
        atributes = [atribute for atribute in self.__dict__ if 
                    (atribute.startswith('cbx_') or atribute.startswith('tc_'))]
        
        for atribute in atributes:
            key = atribute.split('_')[1]
            func = getattr(self, atribute) 
            dicdatos[key] = func.GetValue()
            
        diccionario[user] = dicdatos
        
        self.bt_save.Enable(False)
        return self.scriba.write(diccionario)
    
    def on_new(self, evt):
        ""
        a = self.__dict__
        for key in a:
            if key.startswith('tc_'): #or key.startswith('cbx_'):
                try:
                    att = getattr(self, key)
                    att.Clear()
                except NameError:
                    pass  
#
#
class UserPanelShow(UserPanel):
     
    def __init__(self, parent, memosite, *args, **kwds):
        UserPanel.__init__(self, parent, memosite, *args, **kwds)
        self.bt_save.SetLabel('APPLY')
        self.bt_new.SetLabel('CLEAR')
        self.bt_save.SetToolTipString('Utiliza temporalmente estos valores')
        self.bt_quit.Hide()
        self.Bind(wx.EVT_BUTTON, self.on_apply, self.bt_save)
        self.bt_save.Enable(False)
    
    def on_apply(self, evt):
        ""
        aviso('no implementado')
#
#
class AnalPanel(BaseAnalPanel):
    def __init__(self, parent, memosite, *args, **kwds):
        BaseAnalPanel.__init__(self, parent, *args, **kwds)
        self.list_tipanals_path = memosite + os.sep + ARCH_TIPANAL
        self.anal_dict = {}
        self.dict_tipanals = {}
        self.sin_tarea = True
        self.actualiza = True
        self.nsample_original = 0
        
        self.chbx_free.SetValue(1)
        self.chbx_filter.SetValue(1)
        
        memofile = memosite + os.sep + ARCH_ANAL
        self.scriba = Escriba('anal', memofile)
        self.anal_dict = self.scriba.lee_datos()
        self.freeze = self.chbx_freeze.IsChecked()
        
        self.set_task_choices(None)
        self.set_refint_choices(None)
        self.set_utipo_choices()
        self.set_stincion_choices()
        
        self.protect_writing(None)
        
        self.Bind(wx.EVT_CHECKBOX, self.protect_writing, self.chbx_freeze) 
        self.Bind(wx.EVT_CHECKBOX, self.set_task_choices, self.chbx_free)
        self.Bind(wx.EVT_CHECKBOX, self.set_refint_choices, self.chbx_filter) 
        #
        self.Bind(wx.EVT_COMBOBOX, self.fill_analdata_bytask, self.cbx_task) 
        self.Bind(wx.EVT_COMBOBOX, self.fill_analdata_byref, self.cbx_refint)
        self.Bind(wx.EVT_COMBOBOX, self.set_exp_type, self.cbx_aanal)
        self.Bind(wx.EVT_COMBOBOX, self.fill_analcost, self.cbx_utipo)
        #
        self.Bind(wx.EVT_TEXT, self.calculaimporte, self.tc_acostep)
        self.Bind(wx.EVT_TEXT, self.calculaimporte, self.tc_acostes)
        self.Bind(wx.EVT_TEXT, self.calculaimporte, self.tc_acosten)
        self.Bind(wx.EVT_TEXT, self.calculaimporte, self.tc_asamplep)
        self.Bind(wx.EVT_TEXT, self.calculaimporte, self.tc_asamples)
        self.Bind(wx.EVT_TEXT, self.calculaimporte, self.tc_asamplen)
        #
        self.Bind(wx.EVT_TEXT, self.fill_analcost, self.tc_asamplet)
        #
        self.Bind(wx.EVT_BUTTON, self.limpia_all, self.bt_clearall)
        self.Bind(wx.EVT_BUTTON, self.limpia_com, self.bt_clrcoment)
        self.Bind(wx.EVT_BUTTON, self.limpia_mst, self.bt_clrmuestra)
        #
    
        self.Bind(wx.EVT_BUTTON, self.on_setdate, self.bt_setdate)
        #self.Bind(wx.EVT_BUTTON, self.on_load, self.bt_load)
        self.Bind(wx.EVT_BUTTON, self.on_save, self.bt_save)
        
        self.set_exp_choices()
        self.settime()
    
    def on_setdate(self, evt):
        ""
        fecha = self.tc_dhoy.GetValue()
        self.tc_dfact.SetValue(fecha)
    
    def on_load(self, evt):
        """carga un report asignando la referencia. Deberia hacerlo aparecer
        en la ventana principal pero resulta que notefact es una ventana 
        independiente.
        """
        try:
            self.parent.parent.iewin.ie.LoadUrl(r'C:\datos_xmar\report\MCP_2_080424.htm')
        except:
            aviso('fallo')
            
        aviso('no implementado')
        
    def protect_writing(self, evt):
        ""
        self.freeze = self.chbx_freeze.IsChecked()
        for tc in ['acostep', 'acostes', 'acosten',
                   'asamplep', 'asamples', 'asamplen']:
            func = getattr(self, 'tc_'+ tc)
            func.Enable(self.freeze)
            func.Refresh() 
        self.tc_asamplet.Enable(self.freeze==False)
        self.tc_asamplet.Refresh()
        
    def set_exp_type(self, evt):
        ""
        self.cbx_aanal.SetBackgroundColour("white")
        self.fill_analcost(None)
    
    def fill_analcost(self, evt):
        """
        Por el momento no hay seleccion de escala de coste
        """
        if not self.actualiza: return
        #print '************entro***************'
        muestras = self.tc_asamplet.GetValue()
        utipo = self.cbx_utipo.GetSelection()
        
        if muestras and not muestras.isdigit():
            self.tc_asamplet.SetBackgroundColour('red')
            self.tc_asamplet.Refresh()
            return
        elif not muestras:
            muestras = '0'
            self.tc_asamplet.SetBackgroundColour('white')
            self.tc_asamplet.Refresh()
        #
        if not self.freeze and muestras not in '*':
            anal_selected = self.cbx_aanal.GetStringSelection()
            if anal_selected is not '' and anal_selected in self.dict_tipanals:
                if muestras in '*':
                    aviso('es necesario introducir numero de muestras totales')
                    return
                #
                for tc in ['acostep', 'acostes', 'acosten',
                           'asamplep', 'asamples', 'asamplen']:
                    func = getattr(self, 'tc_'+ tc)
                    func.Clear()
                
                try:
                    datos = calcula_coste(muestras, utipo,
                                       self.dict_tipanals[anal_selected])
                    self.tc_asamplet.SetBackgroundColour('white')
                    self.tc_asamplet.Refresh()
                except IndexError:
                    self.tc_asamplet.SetBackgroundColour('red')
                    aviso('demasiadas muestras...')
                    return
                
                
                self.tc_acostep.SetValue(str(datos[0][1]))
                self.tc_asamplep.SetValue(str(datos[0][0]))
                if len(datos) > 1:
                    self.tc_acostes.SetValue(str(datos[1][1]))
                    self.tc_asamples.SetValue(str(datos[1][0]))
                    if len(datos) > 2:
                        self.tc_acosten.SetValue('%.2f' %datos[2][1])
                        self.tc_asamplen.SetValue('%i' %datos[2][0])
                costet = 0
                for dato in datos:
                    costet += dato[0]*dato[1] 
                self.tc_atotal.SetValue(str(costet))
    
        elif self.freeze:
            self.calculaimporte(None)
                
    
    def calculaimporte(self, evt):
        """
        hay que vigilar la recursividad de esta funcion ya que se activa
        cada vez que se entra un valor con SetValue en cualquiera de las 
        ventanas capaces de dar el evento. Algun problema se ha solucionado con
        flags  'self.actualiza', 'self.freeze'
        """
        try:
            self.tc_atotal.SetBackgroundColour('white')
            self.tc_atotal.Refresh()
            
            mtramo1 = self.dame(self.tc_asamplep.GetValue())
            mtramo2 = self.dame(self.tc_asamples.GetValue())
            mtramon = self.dame(self.tc_asamplen.GetValue())
           
            ctramo1 = self.dame(self.tc_acostep.GetValue())
            ctramo2 = self.dame(self.tc_acostes.GetValue())
            ctramon = self.dame(self.tc_acosten.GetValue())
           
        except ValueError:
            self.tc_atotal.SetBackgroundColour('red')
            self.tc_atotal.Refresh()
            return
            
        c1 = c2 = cn = 0
        if mtramo1 and ctramo1: c1 = mtramo1 * ctramo1
        if mtramo2 and ctramo2: c2 = mtramo2 * ctramo2
        if mtramon and ctramon: cn = mtramon * ctramon
        
        ctotal = '%.1f'%(c1 + c2 + cn)
        
        status_quo = self.actualiza
        self.actualiza = False
        if self.freeze:
            self.tc_asamplet.SetValue(str(int(mtramo1+mtramo2+mtramon)))
        self.actualiza = status_quo
        self.tc_atotal.SetValue(ctotal)
    
    def set_stincion_choices(self):
        choices = ['Silver', 'Coomassie', 'DIGE']
        self.cbx_stincion.SetItems(choices)
    
    def set_refint_choices(self, evt):
        ""
        user = self.cbx_user.GetValue()
        ref = self.cbx_refint.GetValue()
        
        if self.chbx_filter.IsChecked() and user:
            refintchoices = [self.anal_dict[anal]['refint'] for anal
                             in self.anal_dict if self.anal_dict[anal]['user'] == user
                            ]
            refintchoices = [item for item in refintchoices if item]
        else:
            refintchoices = [self.anal_dict[anal]['refint'] for anal in self.anal_dict]
            refintchoices = [item for item in refintchoices if item]
        
        self.cbx_refint.SetItems(refintchoices)
        self.cbx_refint.SetValue(ref)
    
    def set_task_choices(self, evt):
        ""
        if self.chbx_free.IsChecked():
            taskchoices = [self.anal_dict[anal]['task'] for anal in
                           self.anal_dict if self.anal_dict[anal]['refint'] in '*'
                          ]
        else:
            taskchoices = [self.anal_dict[anal]['task'] for anal in self.anal_dict]
        self.cbx_task.SetItems(taskchoices)
        
    def set_exp_choices(self):
        """coge el nombre y los costes de los analisis
           cada analisis es:
           dict_tipanals[tipo]:
          ([(sample_tram1, cost1, cost2, cost3), (sample_tram2, cost1,cost2,cost3),
            (sample_tram3, cost1, cost2, cost3), (sample_tramn,cost1, cost2, cost3)]
           
        inicialmente el coste esta en formato "interno>normal>empresa"
        """
        analisis_list = [analisis.strip().split('\t') for analisis
                        in open(self.list_tipanals_path, 'r')]
        
        for item in analisis_list[1:]: #quito cabecera
            sampleandcost = []
            sampleandcostramos = []
            for tramocode in item[3:7]:
                sampleandcostramos.append(tramocode.split('>'))
            sampleandcost.extend(sampleandcostramos)
            self.dict_tipanals[item[0]] = sampleandcost
        #
        self.exp_choices = [item for item in self.dict_tipanals]
        self.cbx_aanal.SetItems(self.exp_choices)
    
    def set_utipo_choices(self):
        ""
        choices = ['interno', 'externo', 'privado']
        self.cbx_utipo.SetItems(choices)
    
    def fill_analdata_byref(self, evt):
        """busco que analisis/tarea contiene esa referencia y mando rellenar
        con los datos de la tarea
        """
        refint = self.cbx_refint.GetStringSelection()
        if not refint:
            refint = self.cbx_refint.GetValue()
        #
        for anal in self.anal_dict:
            if self.anal_dict[anal]['refint'] == refint:
                self.fill_analdata(self.anal_dict[anal])
                #
                try:
                    evt.Skip()
                except AttributeError:
                    pass
                return

                    
    def fill_analdata_bytask(self, evt):
        "carga una tarea. Si es una tarea ya facturada, avisa"
        SINVALOR = '*'          
        self.actualiza = False
        task = self.cbx_task.GetStringSelection()
        panel_refint = self.cbx_refint.GetValue()
        panel_user = self.cbx_user.GetValue()
        #
        diccionario = self.anal_dict[task]
        new_user = diccionario['user']
        new_refint = diccionario['refint']
        exp_type = diccionario['aanal']
        #
        #si la tarea seleccionada tiene usuario
        #que resulta distinto que el actualmente mostrado
        #y si la tarea no estaba todavia asignada a una refint
        #caso1
        if len(new_user) > 1 and len(panel_user)>1 and len(new_refint)<2 :
            if new_user != panel_user:
                texto = ("Esta tarea tiene un usuario distinto al corriente",
                         "Si continuas lo sobreescribiras",
                         "",
                         "Quieres Continuar?"
                        )
                if not dame_ok(texto):
                    self.actualiza = True
                    self.cbx_task.SetSelection(-1)
                    return
        #
        #Si no habia una tarea anterior (al abrir un nuevo documento)
        #y la tarea seleccionada ya tiene una referencia interna 
        #caso2
        if (self.sin_tarea and (new_refint not in SINVALOR) and
            (panel_refint not in SINVALOR) and (new_refint != panel_refint)):
            texto = ("Esta tarea ya tiene Referencia Interna",
                     "Si continuas sobreescribiras la corriente",
                     "",
                     "Quieres Continuar?"
                    )
            if not dame_ok(texto):
                self.actualiza = True
                self.cbx_task.SetSelection(-1)
                return
        
        self.fill_analdata(diccionario)
        
        if new_refint in SINVALOR:
            self.cbx_refint.SetValue(panel_refint)
        
        if new_user in SINVALOR:
            self.cbx_user.SetValue(panel_user)
            if panel_user in SINVALOR:
                texto = ("Ojo",
                         "Por algun motivo no hay usuario seleccionado"
                        )
                aviso('%s\n%s'%texto)
        
        if exp_type not in self.exp_choices:
            self.cbx_aanal.SetBackgroundColour("light red")
            self.cbx_aanal.Refresh()
            aviso('el tipo de analisis no se ha definido o no existe')
        else:
            self.cbx_aanal.SetBackgroundColour("white")
            self.cbx_aanal.Refresh()
        
        self.actualiza = True
        self.sin_tarea = False
        evt.Skip()                                
    
    def fill_analdata(self, diccionario):
        """extrae el nombre de los widgets de esta clase que contienen
        cbx_ (comboboxes) o tc_ (text_Crtl).
        La clase se ha nombrado ex-profeso utilizando estos codigos
        la key del diccionario de usuarios corresponden con estos atributos
        de forma que puede trabajarse automaticamente
        atributos de la clase -> ['tc_fcdd', 'tc_fdfscl', ... , 'tc_uap2']
        keys del dicionario -> ['fcdd', 'fdfscl', ... , 'uap2']
        """
        atributes = [atribute for atribute in self.__dict__ if 
                    (atribute.startswith('cbx_') or atribute.startswith('tc_'))]

        for key in diccionario:
            a = 'tc_' + key
            b = 'cbx_'+ key
            if a in atributes:
                unbf = getattr(self, a)
                txt = diccionario[key]
                lista = txt.split('|')
                unbf.Clear()
                for item in lista[:-1]:
                    unbf.AppendText(item)
                    unbf.AppendText('\n')
                unbf.AppendText(lista[-1])
                continue
            if b in atributes:
                unbf = getattr(self, b)
                unbf.SetValue(diccionario[key])
          
    
    def settime(self):
        dt = time.localtime(time.time())
        fecha = '%i-%i-%i' %(dt[2], dt[1], dt[0])
        self.tc_dhoy.SetValue(fecha)
        
         
    def dame(self, num):
        "hace un cambio de coma a punto para corregir problema de formato excel"
        if num in '*': num = 0
        try:
            num = float(num)
        except ValueError:
            try:
                num = num.replace(',', '.')
                num = float(num)
            except TypeError:
                num = 0
        return num
        
    def on_save(self, evt):
        "salva el que esta visible...o todos"
        diccionario = {}
        dicdatos = {}
        
        task = self.cbx_task.GetValue()
      
        atributes = [atribute for atribute in self.__dict__ if 
                    (atribute.startswith('cbx_') or atribute.startswith('tc_'))]
        
        for atribute in atributes:
            key = atribute.split('_')[1]
            func = getattr(self, atribute) 
            dicdatos[key] = func.GetValue()
            
        diccionario[task] = dicdatos
        
        return self.scriba.write(diccionario)
    
    
    def limpia_all(self, evt):
        a = self.__dict__
        for key in a:
            if key.startswith('tc_'): #or key.startswith('cbx_'):
                try:
                    att = getattr(self, key)
                    att.Clear()
                except NameError:
                    pass
      
    def limpia_com(self, evt):
        self.tc_comentario.Clear()
        
    def limpia_mst(self, evt):
        self.tc_stipo.Clear()
#
#
class DocumentPanel(BaseDocumentPanel):
    ""
    checkboxes = ['fdoc', 'fpdf', 'fmail', 'fsave', 'fprint',
                      'rdoc', 'rpdf', 'rmail', 'rsave', 'rprint',
                      'factura', 'report', 'proteored']
    
    def __init__(self, *args, **kwds):
        BaseDocumentPanel.__init__(self, *args, **kwds)
        self.set_checkboxes()
        #
        self.freport_path = FORM_REPORT
        self.ffact_path = FORM_FACTURAS
        self.pchoices = PRINTER_CHOICES
        self.pdefault = PRINTER_DEFAULT
        self.server = SMTPSERVER
        self.mfactura = MFACT
        self.mcopy = MCOPY
        #
        self.cbx_printer.SetItems(self.pchoices)
        self.cbx_printer.SetValue(self.pdefault)
        self.tc_mfactura.SetValue(self.mfactura)
        self.tc_mcopy.SetValue(self.mcopy)
        self.tc_server.SetValue(self.server)        
        self.tc_freport.SetValue(self.freport_path)
        self.tc_ffact.SetValue(self.ffact_path)
        
        self.Bind(wx.EVT_BUTTON, self.on_form_report, self.bt_freport)
        self.Bind(wx.EVT_BUTTON, self.on_form_factura, self.bt_ffact)
        
    def set_checkboxes(self, chk_ons = None):
        ""
        if not chk_ons:
            chk_ons = [1,1,0,1,0,     #factura .doc .pdf mail save print
                       1,1,0,1,0,     #report
                       1,0,0          #print   fact  rprt pred
                        ]
      
        for checkbox, value in zip(self.checkboxes, chk_ons):
            checkbox = 'chbx_'+ checkbox 
            atribute = getattr(self, checkbox)
            atribute.SetValue(value)
            
    def lee_checkboxes(self):
        ""
        chk_ons = []
        for checkbox in self.checkboxes:
            func = getattr(self, 'chbx_' + checkbox)
            chk_ons.append(func.IsChecked())
        
        return chk_ons
    
    def on_form_report(self, evt):
        ""
        self.freport_path = get_filepath(self.tc_freport)
        
    def on_form_factura(self, evt):
        ""
        self.ffact_path = get_filepath(self.tc_ffact)    
        
        
class NoteFactFrame(wx.Frame):
    def __init__(self, parent, archivo='',
                       memosite='', iewin=None, *args, **kwds):
        # begin wxGlade: NoteFactFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, parent, *args, **kwds)
        #
        set_globals()
        self.source_arch = archivo
        self.memosite = memosite if memosite else DIRMEMO
        self.iewin = iewin
        self.dir_pdf = DIRPDF
        self.outf_basename = ""
        self.task = ''
        self.refint = ''
        self.user = ''
        self.costext = ''
        self.asamplet = '0'
        #
        self.nbook_facturacion = wx.Notebook(self, -1, style=0)
        #
        self.nbookpan_user = UserPanelShow(self.nbook_facturacion, self.memosite, -1)
        self.nbookpan_anal = AnalPanel(self.nbook_facturacion, self.memosite, -1)
        self.nbookpan_docs = DocumentPanel(self.nbook_facturacion, -1)
        
        self.Bind(wx.EVT_COMBOBOX,
                  self.sinc_user_anal, self.nbookpan_anal.cbx_task)
        
        self.Bind(wx.EVT_COMBOBOX,
                  self.sinc_user_anal, self.nbookpan_anal.cbx_refint)
        
        self.Bind(wx.EVT_COMBOBOX,
                  self.sinc_user, self.nbookpan_anal.cbx_user)
        
        self.Bind(wx.EVT_BUTTON, self.on_make, self.nbookpan_docs.bt_make)
        self.Bind(wx.EVT_BUTTON, self.on_load, self.nbookpan_anal.bt_load)
        
        self.__set_properties()
        self.__do_layout()
        self.set_icon(ICON)
        
        self.init_datos_facturacion()
        userchoices = self.nbookpan_user.cbx_user.GetItems()
        self.nbookpan_anal.cbx_user.SetItems(userchoices)
        self.nbookpan_anal.set_refint_choices(None)
        if self.source_arch:
            self.init_documents()
        
        self.nbookpan_user.bt_save.Enable(False)
    
##    def sinc_user(self, evt):
##        self.user = self.nbookpan_anal.cbx_user.GetValue()
##        utipo = self.nbookpan_user.cbx.utipo.GetValue()
##        self.nbookpan_anal.cbx_utipo.SetStringSelection(utipo)
        
    def on_load(self, evt):
        """carga un report asignando la referencia. Deberia hacerlo aparecer
        en la ventana principal pero resulta que notefact es una ventana 
        independiente.
        """
        fd = wx.FileDialog(None)
    
        if fd.ShowModal() == wx.ID_OK:
            archivo = fd.GetPath()
            fd.Destroy()
        else:
            fd.Destroy()
            return
        
        try:
            self.iewin.ie.LoadUrl(archivo)
            self.source_arch = archivo
            self.init_datos_facturacion()
            self.init_documents()
        except:
            aviso('fallo')
            
    def init_documents(self):
        self.nbookpan_anal.tc_asamplet.SetValue(self.asamplet)
        self.nbookpan_anal.tc_asamplep.SetValue(self.asamplet)
        self.nbookpan_docs.tc_mfactura.SetValue(self.nbookpan_docs.mfactura)
        self.nbookpan_user.fill_data_of_user(self.user)
        self.umail = self.nbookpan_user.tc_umail.GetValue()
        self.nbookpan_docs.tc_mreport.SetValue(self.umail)
        self.nbookpan_anal.cbx_user.SetValue(self.user)
        self.nbookpan_anal.cbx_refint.SetValue(self.refint)
        if self.refint:
            self.nbookpan_anal.fill_analdata_byref(None)
        self.nbookpan_user.cbx_user.SetValue(self.user)
        if self.user not in '*':
            utipo = self.nbookpan_user.cbx_utipo.GetValue()
            self.nbookpan_anal.cbx_utipo.SetStringSelection(utipo)
        self.task = self.nbookpan_anal.cbx_task.GetValue()
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: NoteFactFrame.__set_properties
        title = "FACTURACION  - data in %s" %self.memosite
        self.SetTitle(title)
        self.SetSize((445, 570))
        #self.textctrl2.SetMinSize((424, 472))
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: NoteFactFrame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.nbook_facturacion.AddPage(self.nbookpan_docs, "Documents")
        self.nbook_facturacion.AddPage(self.nbookpan_anal, "Analisis Data")
        self.nbook_facturacion.AddPage(self.nbookpan_user, "Usuario Data")
        sizer_1.Add(self.nbook_facturacion, 1, wx.EXPAND, 0)
        
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def set_icon(self, ico):
        ""
        img = self.decode_string(ico)
        _icon = wx.IconFromBitmap(wx.BitmapFromImage(img))
        self.SetIcon(_icon)
    
    def decode_string(self, string):
        data = base64.decodestring(string)
        flhndl = cStringIO.StringIO(data)
        return wx.ImageFromStream(flhndl, wx.BITMAP_TYPE_ANY)
    
    def sinc_user(self, evt):
        """Cuando se modifica el usuario en la pagina de usuario cuando
        se modifca el usuario en analisis
        """
        user = self.nbookpan_anal.cbx_user.GetStringSelection()
        self.nbookpan_user.fill_data_of_user(user)
        
    def sinc_user_anal(self, evt):
        """
        Se llama por cbx_task y por cbx_refint
        User se lee de file.mht cuando se carga.
        Pero puede ser que se quiera ver un analisis o referencia ya hechos
        o bien cargar un analisis para una referencia nueva.
        En este ultimo caso si el analisis tiene ya usuario, cambia
        el usuario.
        Se coge por selection cuando se seleeciona en task pero debe leerse 
        cunado se ha seleccionado por refint
        """
        task = self.nbookpan_anal.cbx_task.GetStringSelection()
        if not task:      
            task = self.nbookpan_anal.cbx_task.GetValue()
      
        user = self.nbookpan_anal.anal_dict[task]['user']
        if user in '*':
            user = self.nbookpan_anal.cbx_user.GetValue() 
        #
        self.nbookpan_user.fill_data_of_user(user)
        umail = self.nbookpan_user.tc_umail.GetValue()
        self.nbookpan_docs.tc_mreport.SetValue(umail)
        
    def init_datos_facturacion(self):
        """
        Hay que inventar un nombre para el archivo final a generar.
        no debe tener extension .doc/.pdf, ya se la dare.
        puede ser el --> codigo de usuario + fecha (+ 'report|fact')
        otra opcion  --> codigo de usuario + refinterna + ('report|fact')
        la file deberia guardar en el nombre el numero de analisis y la fecha
        p.e. --> OB_2_080223.htm
        entonces la referencia interna podria ser la fecha
        --> #080223/OBC permite ordenamiento por excel por orden de fecha

        """
        if self.source_arch:
            dirpath, filename = os.path.split(self.source_arch)
            filepieces = filename.split('_')
            self.user = filepieces[0]
            self.asamplet = filepieces[1]
            #
            date = '%s%02d%02d' %tuple(time.localtime(time.time())[0:3])
            self.refint = '#%s/%s' %(date[2:], self.user)
            self.outf_basename = filename.split('.')[0]
        else:
            pass
             
    def on_make(self, evt):
        "Aqui viene la chicha"
        
        # [1,1,0,1,0,     #factura .doc .pdf mail save print
        #  0,0,0,0,0,     #report
        #  1,0,0          #print   fact  rprt pred
        
        name_archivo = DIRQUIM + os.sep + self.outf_basename
        name_documento = self.source_arch
        #
        mreport = self.nbookpan_docs.tc_mreport.GetValue()
        mcopy = self.nbookpan_docs.tc_mcopy.GetValue()
        mfact = self.nbookpan_docs.tc_mfactura.GetValue()
        #
        mreport_full = mreport.split() + mcopy.split()
        mfact_full = mfact.split() + mcopy.split()
        #
        printer = self.nbookpan_docs.cbx_printer.GetValue()
        server = self.nbookpan_docs.tc_server.GetValue()    
        #
        self.chkbx_status = self.nbookpan_docs.lee_checkboxes()
        #
        facturar = self.chkbx_status[10]
        reportar = self.chkbx_status[11]
        proteoredar = self.chkbx_status[12]
      
        factura_doc = self.chkbx_status[0]
        factura_pdf = self.chkbx_status[1]
        mailfactura = self.chkbx_status[2]
        salvarfactura = self.chkbx_status[3]
        printarfactura = self.chkbx_status[4]
        
        report_doc = self.chkbx_status[5]
        report_pdf = self.chkbx_status[6]
        mailreport = self.chkbx_status[7]
        salvarreport = self.chkbx_status[8]
        printarreport = self.chkbx_status[9]
        
        marcas = self.get_marcas(codes_user + codes_anal)
        self.costext = self.get_frase_coste()
        marcas.append(('.FRASECOSTE.', self.costext))
        #
        if facturar:
            factura = WordDoc(self.nbookpan_docs.ffact_path)
            factura.substituye_marcas(marcas)
            #
            if salvarfactura:
                if factura_doc:
                    factura.saveas(name_archivo + "_fac.doc")
                if factura_pdf:
                    if not factura_doc: factura.saveas(name_archivo + "_fac.doc")
                    factura.printa(printer, pdf=True)
                    time.sleep(3)   #ojo, sin esto se para todo por quitcancel
            #
            if mailfactura:
                sender = SENDER
                recipient = mfact_full
                subject = 'Factura ' + self.refint
                text = TEXTFACTURA
                attach = [self.dir_pdf + os.sep + "Microsoft Word - " + self.outf_basename + "_fac.pdf"]
                mail(sender, recipient, subject, text, attach, server)
            #
            if printarfactura:
                aviso("!!!! PRINTA FACTURA? A mi que me dices !!!")
                
            factura.close()
            #factura.quitcancel()
            
        if reportar:
            report = WordDoc(self.nbookpan_docs.freport_path)
            report.substituye_marcas(marcas)
            report.inserta_doc(2, name_documento)
            report.haz_magia()
            #
            if salvarreport:
                if report_doc:
                    report.saveas(name_archivo + "_rep.doc")
                if report_pdf:
                    if not report_doc: report.saveas(name_archivo + "_rep.doc")
                    report.printa(printer, pdf=True)
                    time.sleep(3)
            #
            if mailreport:
                sender = SENDER
                recipient = mreport_full
                subject = 'Report ' + self.refint
                text = TEXTREPORT
                attach = [self.dir_pdf + os.sep + "Microsoft Word - " + self.outf_basename + "_rep.pdf"]
                #print 'MAIL= ', sender, recipient, attach
                mail(sender, recipient, subject, text, attach, server)
            #
            if printarreport:
                aviso("!!!! MAIL REPORT? A mi que me dices !!!")
            
            report.close()          #no cierra words abiertos
            #report.quitcancel()    #posiblemente cierra words abiertos
            
        if proteoredar:
            self.genera_xcel_proteored()
        
    
    def genera_xcel_proteored(self):
        """debe generar un archivo separado por tabs conteniendo los parametros
        que se utilizan para subir a proteored
        #
        c1  c2  c3      c4          c5          
        	6	P�blica	E. Ba��n	LP CSIC/UAB
        #
        c6(servicio)
        Peptide Mass Fingerprinting by MS-MALDI TOF
        #
        c7  c8(dfact)   c9      c10(analisis)
        36	28/04/2007	921,20	Peptide Mass Fingerprinting by MS-MALDI TOF
        #
        c11                                     c12(drecep) c13     c14(danal)
        5 x 28  c/una, resto x 25'2  c/una	    19/02/2007	IIBB	20/02/2007
        """
        C1 = '*'
        C2 = '*'
        C3 = self.nbookpan_anal.cbx_utipo.GetValue()
        C4 = self.nbookpan_user.tc_uname.GetValue() +\
                ' ' + self.nbookpan_user.tc_uap1.GetValue()
        C5 = self.nbookpan_user.tc_dcentro.GetValue().split('\n')[0]
        
        analisis_list = [analisis.strip().split('\t') for analisis
                        in open(self.nbookpan_anal.list_tipanals_path, 'r')]
        for item in analisis_list[1:]:
            if item[0] == self.nbookpan_anal.cbx_aanal.GetValue():
                analisis = item[1]
                servicio = item[2]
                break
        C6 = analisis
        C7 = self.nbookpan_anal.tc_asamplet.GetValue() 
        C8 = self.nbookpan_anal.tc_dfact.GetValue() 
        C9 = self.nbookpan_anal.tc_atotal.GetValue()
        C10 = servicio
        C11 = ' '.join(self.costext.split('\n'))
        C12 = self.nbookpan_anal.tc_drecep.GetValue()
        C13 = '?'
        C14 = self.nbookpan_anal.tc_danal.GetValue()
        
        lista = [C1, C2, C3, C4, C5, C6, C7,
                 C8, C9, C10,C11, C12, C13, C14
                ]
        text = '\t'.join(lista)
              
        try:
            hndl = open(ARCH_PROTEORED, 'r+')
            oldtxt = hndl.read()
            newtxt = oldtxt + '\n' + text
            newtxt = newtxt.strip()
            hndl.seek(0)
            hndl.write(newtxt)
            hndl.close()
        except IOError:
            hndl.close()
        
        
    def get_marcas(self, usar):
        ""    
        code_list = []
        
        codes_user = ['user', 'utreat', 'uname',  'uap1', 'uap2', 'umail',
               'dcentro', 'rname', 'tlfn1', 'tlfn2', 'rmail',
               'fname', 'fdfscl', 'fcdd', 'fps', 'fcp', 'pryct', 'nif']

        codes_anal = ['refint',	'user',	'drecep', 'ddigest', 'danal',
                      'dreport', 'dfact', 'stipo', 'sproc', 'stincion',
                      'aanal', 'acostep', 'acostes', 'acosten', 'asamplet',
                      'asamplep','asamples', 'asamplen', 'atotal', 'comentario']
                
        for code in usar:
            mark = '.%s.' %code.upper()
            
            if code in codes_user:
                win = self.nbookpan_user
            else:
                win = self.nbookpan_anal
            
            a = 'cbx_'+ code
            b = 'tc_'+ code
            
            try:
                func = getattr(win, a)
            except AttributeError:
                func = getattr(win, b)
            
            value = func.GetValue()
            code_list.append((mark, value))
        
        return code_list
    
    def get_frase_coste(self):
        """Habria que asegurar que no se puedan producir errores
        derivados de un rellenado manual mal hecho o equivocado
        """
        codes_coste = ['acostep', 'acostes', 'acosten']
        codes_sample = ['asamplep','asamples', 'asamplen']
        datos = []            
        for coste, sample in zip(codes_coste, codes_sample):
            func_coste = getattr(self.nbookpan_anal, 'tc_'+ coste)
            func_sample = getattr(self.nbookpan_anal, 'tc_'+ sample)
            try:
                _coste = float(func_coste.GetValue())
                _sample = int(func_sample.GetValue())
            except ValueError:
                break
            datos.append([_sample, _coste])
        
        return genera_txt_costes(datos)
            
       
# end of class NoteFactFrame

class UserFrame(wx.Frame):
    def __init__(self, parent, memo, *args, **kwds):
        wx.Frame.__init__(self, parent, *args, **kwds)
        memosite = memo if memo else DIRMEMO
        self.SetTitle("UserBase  - " + memosite)
        self.SetSize((420, 530))
        self.pan = UserPanel(self, memosite)
        self.Bind(wx.EVT_BUTTON, self.on_quit, self.pan.bt_quit)
        self.pan.bt_save.Enable(False)
    
    def on_quit(self, evt):
        ""
        self.Destroy()
        

class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        ntfact = NoteFactFrame(None, "MCP_2_08234.htm", DIRMEMO, None, -1, "")
        userframe = UserFrame(None, DIRMEMO)
        self.SetTopWindow(ntfact)
        userframe.Show()
        ntfact.Show()
        return 1

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
