#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
xmr_cls_baseframe_06
(xmr_15)  28 de abril de 2008
"""
#
import time
import os
import wx
import base64
import cStringIO
import wx.lib.iewin as iewin
from xmr_funcs_11 import get_filepath, aviso
from xmr_help_01 import HelpFrame
from xmr_images import ICON
#
TITULO = ' XMAR vs.1.5'
#
wildcard = "HTM (*.htm)|*.htm|"  \
           "HTML (*.html)|*.html|"  \
           "Texto (*.txt)|*.txt|"   \
           "XML (*.xml)|*.xml"
#
STYPE = 'Tipo de busqueda identidad/homologia'
DBASE = 'Database en el servidor. Actualmente solo disponible SwissProt'
INSTRUMENT = 'Tipo de instrumento utilizado'
DNA_FRAME = 'DNA frame translation. 6 son todas las posibilidades'
SPECIES = 'Restringe la busqueda a las especies indicadas'
LOWMASS = 'Masa minima de la proteina'
HIGHMASS = 'Masa maxima de la proteina'
FULL = 'Buscar sin restrincion de tamaño 0/1'
ENZYME = 'Enzima utilizada para el mapeo'
MISSED = 'Numero maximo de puntos de corte remanentes en una secuencia (digestion parcial)'
NTERM = 'Tipo de N-terminal'
CTERM = 'Tipo de C-Terminal'
CYS = 'Modificacion de la cisteina'
PMT = 'Tolerancia en la masa de iones derivada del instrumento'
MPIM = 'Numero minimo de iones matcheados en un hit'
NOSE = ''
MAXHITS = 'Numero maximo de hits devueltos por el buscador'
UNITS = 'Unidades de medida de la tolerancia'
MOWSEFACT = ''
MOD = 'Posibles modificaciones postraduccionales'
PMC = ''
#
listado_dbase_flnmes = [DBASE, 'SwissProt.fasta','SwissProt.fasta']
#
def parData():
    variables = {
        'search_type':[STYPE,'identity', 'identity', 'homology'],
        'database'   :listado_dbase_flnmes,
        'instrument_name':[INSTRUMENT,'MALDI-TOF','MALDI-TOF', 'ESI-ION-TRAP'],
        'dna_frame_translation' :[DNA_FRAME,'6', '6','3','-3','1', '-1'],
        'species' :[SPECIES, 'MAMMALS', 'All', 'HOMO SAPIENS', 'HUMAN MOUSE',
                             'MAMMALS', 'RODENT', 'NOT HUMAN'],
        'prot_low_mass'   :[LOWMASS,'1000','1000','10000','100000'],
        'prot_high_mass'  :[HIGHMASS, '100000', '1000','10000', '100000','1000000'],
        'enzyme':[ENZYME, 'Trypsin', 'Trypsin','Arg-C','Lys-C'],
        'missed_cleavages':[MISSED, '1','0','1', '2'],
        'nterm':[NTERM, 'Hydrogen','Hydrogen', 'Acetyl'],
        'cterm':[CTERM, 'Free Acid', 'Amide'],
        'cys':  [CYS, 'carbamidomethylation','acrylamide','unmodified',
                    'carboxymethylation','carbamidomethylation'],
        'parent_mass_tolerance'   :[PMT, '100','100', '10','20','50','100'],
        'min_parent_ion_matches'  :[MPIM,'4', '4', '5','10','15'],
        'max_reported_hits': [MAXHITS, '10', '1', '3', '5', '10', '20'],
        'tolerance_units' : [UNITS, 'ppm', 'Da', 'ppm'],
        'mowse_pfactor': [MOWSEFACT, '0.4','0.01', '0.4', '1'],
        'mod_AA': [MOD, '', '', 'Peptide N-terminal Gln to pyroGlu',
                        'Oxidation of M', 'Protein N-terminus Acetylated',
                        'User Defined 1', 'Acrylamide Modified Cys'],
        'parent_mass_convert': [PMC, 'monoisotopic', 'monoisotopic', 'average']	
                }
    return variables
#
class FileScreen(wx.Panel):
    def __init__(self, parent, label='', value='', name='', ID=-1,
                   pos=wx.DefaultPosition, size=(200, 22)):
        wx.Panel.__init__(self, parent, ID, pos, size, name=name)
        
        self.SetBackgroundColour(wx.Colour(253, 255, 164))
        self.SetMinSize(size)
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        flag = wx.ALIGN_CENTER|wx.ALL
        #
        self.bw = wx.StaticText(self, size=(85,25),label=label)
        self.tc = wx.TextCtrl(self, style=wx.TE_READONLY)
        self.bsel = wx.Button(self, size=(21,17), label='...', name=name)
        #
        sizer.Add(self.bw,0, wx.ALL, 2)
        sizer.Add(self.tc, 1, wx.TOP|wx.BOTTOM|wx.EXPAND, 2)
        sizer.Add(self.bsel, 0, wx.TOP|wx.BOTTOM, 2)
        #
        self.SetSizer(sizer)
        self.Fit()
        #
        self.Bind(wx.EVT_BUTTON, self.on_select, self.bsel)
        
        if value: self.tc.SetValue(value)
        
    def on_select(self, evt):
        evt.Skip()
     
class DirWindow(wx.Panel):
    def __init__(self, parent, basedir, ID=-1, pos=wx.DefaultPosition, size=(200, 25)):
        wx.Panel.__init__(self, parent, ID, pos, size, wx.RAISED_BORDER)
        
        self.SetBackgroundColour("light orange")
        self.SetMinSize(size)
        self.SetWindowStyle(wx.BORDER_NONE)
        sizer = wx.BoxSizer(wx.VERTICAL)
        flag = wx.ALIGN_CENTER
        #
        dmaldi= basedir
        ddata = basedir
        drprt = basedir + os.sep + 'report'
        dhtms = basedir + os.sep + 'result'
        #
        self.lbl_fuente = wx.StaticText(self, label='Files/Directorios Fuente',
                                   style=wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL,
                                    size=(10,5))
        self.lbl_results = wx.StaticText(self, label='Directorios Resultados',
                                   style=wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL,
                                   size=(10,5))                  
        self.dmaldi = FileScreen(self, 'Espectros Maldi', value=dmaldi, name='dmaldi')
        self.ddata = FileScreen(self, 'Datos Prospector', value=ddata, name='ddata')
        self.drprt = FileScreen(self, 'Reports', value=drprt, name='drprt')
        self.dhtms = FileScreen(self, 'Prospector Files', value=dhtms, name='dhtms')
        #
        self.lbl_fuente.SetBackgroundColour("yellow")
        self.lbl_results.SetBackgroundColour("yellow")
        #
        sizer.Add(self.lbl_fuente, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 3)
        sizer.Add(self.dmaldi, 1, wx.EXPAND)
        sizer.Add(self.ddata, 1, wx.EXPAND)
        sizer.Add(self.lbl_results, 1, wx.EXPAND|wx.TOP|wx.BOTTOM, 3)
        sizer.Add(self.drprt, 1, wx.EXPAND)
        sizer.Add(self.dhtms, 1, wx.EXPAND)
        
        self.SetSizer(sizer)
        self.Fit()
        
        self.Bind(wx.EVT_BUTTON, self.on_select)
    
    def on_select(self, evt):
        evt.Skip()
        
     
class SendQueryWindow(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=(250, 100)):
        wx.Panel.__init__(self, parent, ID, pos, size, wx.RAISED_BORDER)
        #
        self.SetBackgroundColour("lightorange")
        self.SetWindowStyle(wx.BORDER_NONE)
        self.SetMinSize(size)
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizerv = wx.BoxSizer(wx.VERTICAL)
        #
        SETTINGS = 'Indicar #hits a reportar, cutoff MOWSE, etc '
        #
        self.tc = wx.TextCtrl(self, size=(180, 100), style= wx.TE_MULTILINE) #controla tamaño
        self.sqb = wx.Button(self, size=(90, 35), label='SEND QUERY')
        self.stb = wx.Button(self, size=(90, 23), label='Settings')
        self.clb = wx.Button(self, size=(90, 23), label='Clear')
        self.tc.SetBackgroundColour(wx.Colour(240, 235, 240))
        self.sqb.SetBackgroundColour("green")
        #
        self.stb.SetToolTipString(SETTINGS)
        #
        sizerv.AddSpacer((3,5))
        sizerv.Add(self.sqb, 0, wx.EXPAND)
        sizerv.AddSpacer((3,5))
        sizerv.Add(self.stb, 0)
        sizerv.AddSpacer((3,3))
        sizerv.Add(self.clb, 0)
                    
        sizer.Add(self.tc, 1, wx.EXPAND)
        sizer.AddSpacer((4,2))
        sizer.Add(sizerv, 0, wx.EXPAND)
        #
        self.SetSizer(sizer)
        self.Fit()
        #
        self.Bind(wx.EVT_BUTTON, self.on_clear, self.clb)
        
    def on_clear(self, evt):
        self.tc.Clear()
        
        
class ServidorWindow(wx.Panel):
    def __init__(self, parent, directorio, ID=-1, pos=wx.DefaultPosition, size=(200, 25)):
        wx.Panel.__init__(self, parent, ID, pos, size, wx.RAISED_BORDER)
        
        self.SetBackgroundColour("light orange")
        self.SetWindowStyle(wx.BORDER_NONE)
        self.SetMinSize(size)
        self.schoices = ['local', 'UCSF']
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        flag = wx.ALIGN_CENTER
        #
        OFL = "Selecciona una sola file en vez de un directorio"
        FIO = "Manda a Protein Prospector un espectro\n filtrado segun valores en settings"
        USV = "Busqueda en el servidor de Protein Prospector que se indica"
        TST = "Chequea si hay contacto con el servidor"
        #
        self.chkonefl = wx.CheckBox(self, label='One file')
        self.chkfilter = wx.CheckBox(self, label='Filter Ions')
        self.chkusrvr = wx.CheckBox(self, label='Use Server')
        self.chcsrvr = wx.ComboBox(self, choices=self.schoices, size=(64,20))
        self.bchk = wx.Button(self, size=(25,18), label='test')
        #
        self.chkonefl.SetToolTipString(OFL)
        self.chkfilter.SetToolTipString(FIO)
        self.chkusrvr.SetToolTipString(USV)
        self.bchk.SetToolTipString(TST)
        #
        sizer.Add(self.chkonefl, 0, flag)
        sizer.AddSpacer((10,10))
        sizer.Add(self.chkfilter, 0, flag)
        sizer.AddStretchSpacer()
        sizer.Add(self.chkusrvr, 0, flag)
        sizer.AddSpacer((5,10))
        sizer.Add(self.chcsrvr, 0, flag)#|wx.EXPAND)
        sizer.Add(self.bchk, 0, flag)
        #
        self.SetSizer(sizer)
        self.Fit()
        #
        self.chcsrvr.SetValue(directorio)
        
    def on_search(self, evt):
        texto = self.tc2.GetValue()
        if texto == '':
            self.tc2.SetValue(texto)
            return
        if texto not in self.schoices:
            self.schoices.append(texto)
            self.tc2.SetItems(self.schoices)
            self.tc2.SetValue(texto)
        evt.Skip()
#
#
class DictSelectWindow(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=(680, 98)):
        wx.Panel.__init__(self, parent, ID, pos, size)
        let = (80,36)
        num = (82,16)
        self.SetBackgroundColour("yellow")
        self.SetMinSize(size)
        self.sizer = wx.GridSizer(rows=4, cols=8, hgap=3, vgap=1)
        self.properties = {}
        self.crea_param_selector(num,let)
       
        self.SetSizer(self.sizer)
        self.Fit()
               
    def crea_param_selector(self, num, let):
        flag1 = wx.ALIGN_LEFT
        flag3 = wx.ALIGN_CENTER
        diccionario = parData()
        for eachLabel, eachlist in diccionario.iteritems():
            #print eachLabel, eachlist
            self.sizer.Add (wx.StaticText(self, label='  '+eachLabel[0:13], size=let, style=flag1))
            property = wx.ComboBox(self, -1, eachlist[1],  choices=eachlist[2:],
                                   size=num, style=wx.CB_DROPDOWN)
            property.SetToolTipString(eachlist[0])
            self.sizer.Add(property, 1, flag3)
            self.properties[eachLabel] = property
    
    def refresh_dbchoices(self, choices, default=''):
        #DICT_DTBS --> human_p:human.aa,bhuman.psq,human_p.idx
        if default not in choices:
            default = choices[0]
       
        self.properties['dbase'].SetItems(choices)
        self.properties['dbase'].SetValue(default)
     
     
class CheckBoxes(wx.Panel):
    def __init__(self,parent, ID=-1):
        wx.Panel.__init__(self,parent, ID)
        self.properties = {}
        self.SetWindowStyle(wx.BORDER_STATIC)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.crea_chkbox()
        self.SetSizer(self.sizer)
        self.Fit()
        
    def crea_chkbox(self):
        ""
        flag = wx.ALIGN_CENTER_HORIZONTAL|wx.LEFT
        FULL_PI = 'Search with full pI range'
        FULL_Mr  = 'Search with full pI range'
        NA_1 = 'No asignado'
        NA_2 = 'No asignado'
        
        listado = [(FULL_PI, 'pI  full range', 1), (FULL_Mr, 'Mw full range', 0),
                   (NA_1, '-', 0), (NA_2, '-', 0)]
        
        for tip, label, state in listado:
            property = wx.CheckBox(self, -1, label, size=(83,24), style=flag)
            property.SetValue(state)
            self.sizer.Add(property, 0, flag, 2)
            property.SetToolTipString(tip)
            self.properties[label]=property
#
#
class TaskButtons(wx.Panel):
    def __init__(self, parent, ID=-1, size=(100, 50)):
        wx.Panel.__init__(self,parent, ID, size)
        self.properties = {}
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.crea_botones()
        self.SetSizer(self.sizer)
        self.Fit()
        
    def crea_botones(self):
        ""
        USER = 'Entrada nuevos usuarios'
        MEMOSITE = 'Indica localizacion de archivos de usuarios y de analisis'
        DOCS = 'Preparar, printar y enviar facturas y reports'
        HELP='Ayuda y TO-DO en construccion'
        
        flag1 = wx.ALIGN_CENTER_HORIZONTAL|wx.ALL
        flag2 = wx.ALIGN_CENTER_VERTICAL
        listado=[(DOCS,'DOCUMENTS'),  (USER, 'USER BASE'),
                 (MEMOSITE, 'MEMOSITE'), (HELP, 'HELP'), ]
        for eachTip, eachLabel in listado:
            property = wx.Button(self, -1, eachLabel, size=(75, 22), style=flag2)
            self.sizer.Add(property, 0, flag1, 1)
            property.SetToolTipString(eachTip)
            self.properties[eachLabel] = property
                           
class ParamWindow(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=(600, 105)):
        wx.Panel.__init__(self, parent, ID, pos, size, wx.RAISED_BORDER)
        self.SetBackgroundColour("yellow")
        self.SetWindowStyle(wx.BORDER_SIMPLE)
        self.SetMinSize(size)
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        #
        self.sizer.Add(wx.Panel(self,-1, size=(5,10)))
        self.leftw = DictSelectWindow(self)
        self.centerw = CheckBoxes(self)
        self.rightw = TaskButtons(self)
        #
        self.sizer.Add(self.leftw, 0, wx.TOP, 2)
        self.sizer.Add(wx.Panel(self, -1, size=(5,10)))
        self.sizer.Add(self.centerw, 0, wx.TOP, 2)
        self.sizer.Add(wx.Panel(self, -1, size=(5,10)))
        self.sizer.Add(self.rightw, 1, wx.ALIGN_CENTER_VERTICAL)
        self.sizer.AddSpacer((5,10))
        self.SetSizer(self.sizer)
        self.Fit() 
#
#
class HtmlWin(wx.Panel):
    def __init__(self, parent, dirbase, ID=-1, pos=wx.DefaultPosition, size=(300, 325)):
        wx.Panel.__init__(self, parent, ID, pos, size, wx.RAISED_BORDER)
        #
        self.diropen = dirbase
        self.got = False
        self.current = "http://localhost/"
        #self.current = "http://158.109.210.59/ucsfhtml3.2/msfit.htm"
        botones= {"Stop":self.on_stop,
                  "Open":self.on_open, "Home":self.on_home,
                  "<--":self.on_prev_page, "-->":self.on_next_page,
                  "Refresh":self.on_refresh}
        self.URList = ["http://158.109.210.59/prospector/ucsfhtml3.2/msfit.htm",
                  "http://localhost/", "http://www.python.org/",
                  "http://www.biopython.org/","http://130.14.29.110/BLAST/",
                  "end"]
        #
        self.SetWindowStyle(wx.BORDER_NONE)
        sizer = wx.BoxSizer(wx.VERTICAL)
        btnSizer = wx.BoxSizer(wx.HORIZONTAL)
        #
        self.ie = iewin.IEHtmlWindow(self, -1, style = wx.NO_FULL_REPAINT_ON_RESIZE)
        self.log = wx.TextCtrl(self,-1)
        #
        for boton, funcion in botones.iteritems():
            btn = wx.Button(self, -1, boton, style=wx.BU_EXACTFIT)
            self.Bind(wx.EVT_BUTTON, funcion, btn)
            btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL,1)
        #
        txt = wx.StaticText(self, -1, " Location: ", style=wx.ALIGN_RIGHT)#, size=(20,20))
        #size de location determna el ancho inicial de la frame
        self.location = wx.ComboBox(self, -1, "", choices= self.URList, size=(120,20),
                                    style=wx.CB_DROPDOWN|wx.PROCESS_ENTER)
        bsw = wx.Button(self, -1, 'Save View', style=wx.BU_EXACTFIT)
        self.bs_xcl = wx.Button(self, -1, 'Send to Xcl', style=wx.BU_EXACTFIT)
        pan=wx.Panel(self, -1)
        #
        btnSizer.Add(txt, 4, wx.CENTER|wx.ALL, 2)
        btnSizer.Add(self.location, 10, wx.ALL, 2)
        btnSizer.Add(bsw, 0, wx.EXPAND|wx.ALL,2)
        btnSizer.Add(self.bs_xcl, 0, wx.EXPAND|wx.ALL,2)
        btnSizer.Add(pan, 4, wx.EXPAND|wx.ALL,2)
        #
        sizer.Add(btnSizer, 0, wx.EXPAND)
        sizer.Add(self.ie, 1, wx.EXPAND)
        sizer.Add(self.log, 0, wx.EXPAND)
        #
        self.Bind(wx.EVT_COMBOBOX, self.OnLocationSelect, self.location)
        self.location.Bind(wx.EVT_KEY_UP, self.OnLocationKey)
        self.location.Bind(wx.EVT_CHAR, self.IgnoreReturn)
        self.Bind(wx.EVT_BUTTON, self.on_save_win, bsw)
        self.Bind(wx.EVT_BUTTON, self.on_save_xls, self.bs_xcl)
        self.Bind(wx.EVT_SIZE, self.on_size)
        #
        self.SetSizer(sizer)
        self.ie.AddEventSink(self)
        #
        self.setie()
        
    def setie(self):
##        failtext = "No se puede mostrar la p"
##        buscamas = True
##        print failtext
##        while buscamas:
##            self.ie.LoadUrl(self.current)
##            print 'loaded'
##            while not self.ie.GetText():
##                time.sleep(2)
##            print 'sali'
##            if failtext in self.ie.GetText():
##                newurl = self.getnexturl()
##                if newurl == 'end': break
##                self.current = newurl 
##                
##            else:
##                buscamas = False
        #
        self.ie.LoadUrl(self.current)
        self.location.Append(self.current)
        print self.ie.GetText()             #no funciona aqui. En consola si...
        #
        
    def getnexturl(self):
        for URL in self.URList:
            yield URL
        
    def on_size(self, evt):
        self.Layout()

    def OnLocationSelect(self, evt):
        url = self.location.GetStringSelection()
        self.log.write('OnLocationSelect: %s\n' % url)
        self.ie.LoadUrl(url)

    def OnLocationKey(self, evt):
        #print 'en on location key, ln387'
        if evt.KeyCode == wx.WXK_RETURN:
            URL = self.location.GetValue()
            self.location.Append(URL)
            self.ie.Navigate(URL)
        else:
            evt.Skip()

    def IgnoreReturn(self, evt):
        if evt.GetKeyCode() != wx.WXK_RETURN:
            evt.Skip()

    def on_open(self, event):
        ""
        if os.path.exists(self.diropen) and os.path.isdir(self.diropen):
            directorio = self.diropen
        else:
            directorio = os.getcwd()
            
        dlg = wx.FileDialog(self, message="Choose a file", defaultDir=directorio, 
              defaultFile="", wildcard=wildcard, style=wx.OPEN | wx.CHANGE_DIR)
            
        if dlg.ShowModal() == wx.ID_OK:
            self.current = dlg.GetPath()
            self.ie.Navigate(self.current)
   
        dlg.Destroy()

    def on_home(self, event):
        self.ie.GoHome()

    def on_prev_page(self, event):
        self.ie.GoBack()

    def on_next_page(self, event):
        self.ie.GoForward()

    def on_stop(self, evt):
        self.ie.Stop()

    def on_refresh(self, evt):
        self.ie.Refresh(iewin.REFRESH_COMPLETELY)

    def on_save_win(self, evt):
        import shutil
        trgtpth = self.location.GetValue()
        document = self.ie.locationurl
        #
        if document.startswith('file:'):
            srcpth = document[8:]
        else:
            aviso("solo grabo URLs que empiezan por 'file'")
            return
        if shutil._samefile(srcpth, trgtpth):
            aviso('misma file')
            return
        shutil.copyfile(srcpth, trgtpth)
        self.location.Append(trgtpth)
    
    def on_save_xls(self, evt):
        ""
        evt.Skip()
     
    def logEvt(self, evt):
        pst = ""
        for name in evt.paramList:
            pst += " %s:%s " % (name, repr(getattr(evt, name)))
        self.log.Clear()
        self.log.write('%s: %s' % (evt.eventName, pst))
               
    def BeforeNavigate2(self, this, pDisp, URL, Flags, TargetFrameName,
                        PostData, Headers, Cancel):
        ""
        if '**' in URL[0]:
            print "no pasa nada"
            Cancel[0] = True  
                
    def NewWindow3(self, this, pDisp, Cancel, Flags, urlContext, URL):
        #self.logEvt(evt)
        Cancel[0] = True   

    def OnProgressChange(self, evt):
        self.logEvt(evt)
          
    def DocumentComplete(self, this, pDisp, URL):
        self.logEvt(evt)
        self.current = evt.URL
        self.location.SetValue(self.current)
        
               
class BaseFrame(wx.Frame):
    def __init__(self, dirbase, title, pos=wx.DefaultPosition, size=(690,550)):
        wx.Frame.__init__(self, None, -1, title=title, pos=pos, size=size)
        self.diccionario = {}
        self.dirbase = dirbase
        self.dir_maldi = ''
        self.dir_data  = ''
        self.dir_report= ''
        self.dir_htms  = ''
        self.value={}
        #
        self.init_todo()
        #
        self.SetBackgroundColour(wx.Colour(253, 255, 164))
        #
        sizerh = wx.BoxSizer(wx.HORIZONTAL)
        sizerv1 = wx.BoxSizer(wx.VERTICAL)
        sizerv2 = wx.BoxSizer(wx.VERTICAL)
        #
        self.SetMinSize((644,550))
        #
        self.serverwin = ServidorWindow(self, 'local')
        self.sendwin = SendQueryWindow(self)
        self.dirwin = DirWindow(self, self.dirbase)
        self.pw = ParamWindow(self)
        self.tc = HtmlWin(self, self.dirbase)
        #
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        self.Bind(wx.EVT_BUTTON, self.on_send, self.sendwin.sqb)
        self.Bind(wx.EVT_BUTTON, self.on_settings, self.sendwin.stb)
        #
        self.Bind(wx.EVT_BUTTON, self.on_user, self.pw.rightw.properties['USER BASE'])
        self.Bind(wx.EVT_BUTTON, self.on_set_memosite, self.pw.rightw.properties['MEMOSITE'])
        self.Bind(wx.EVT_BUTTON, self.on_documents, self.pw.rightw.properties['DOCUMENTS'])
        self.Bind(wx.EVT_BUTTON, self.on_help, self.pw.rightw.properties['HELP'])
        #
        self.Bind(wx.EVT_BUTTON, self.on_open_in_xls, self.tc.bs_xcl)
        self.Bind(wx.EVT_BUTTON, self.on_select, self.dirwin.ddata.bsel)
        self.Bind(wx.EVT_BUTTON, self.on_select, self.dirwin.dmaldi.bsel)
        self.Bind(wx.EVT_BUTTON, self.on_select, self.dirwin.dhtms.bsel)
        self.Bind(wx.EVT_BUTTON, self.on_select, self.dirwin.drprt.bsel)
        #
        self.Bind(wx.EVT_BUTTON, self.on_check, self.serverwin.bchk)
        #
        self.Bind(wx.EVT_CHECKBOX, self.on_useserver, self.serverwin.chkusrvr)
        
        flag=wx.EXPAND
        #
        sizerv1.Add(self.serverwin, 0, flag|wx.ALL)
        sizerv1.Add(self.sendwin, 0, flag)
        #
        sizerh.Add(self.dirwin, 2, flag|wx.ALL, 4)
        sizerh.Add(sizerv1, 3, flag|wx.ALL, 4)
        #
        sizerv2.Add(sizerh, 0, flag)
        sizerv2.Add(self.pw, 0, flag|wx.LEFT|wx.RIGHT, 4)
        sizerv2.Add(self.tc,1, flag)
        #
        self.SetSizer(sizerv2)
        self.Fit()
        #
        self.set_icon(ICON)
        #
        self.SetSize((890,500))    #version 1 (PC)
        #
        self.logger = self.sendwin.tc
        self.read_params()
      
    def init_todo(self):
        ""
        try:
            os.mkdir(self.dirbase)
        except OSError:
            pass
    
    def set_icon(self, ico):
        ""
        img = self.decode_string(ico)
        _icon = wx.IconFromBitmap(wx.BitmapFromImage(img))
        self.SetIcon(_icon)
    
    def decode_string(self, string):
        data = base64.decodestring(string)
        flhndl = cStringIO.StringIO(data)
        return wx.ImageFromStream(flhndl, wx.BITMAP_TYPE_ANY)
    
    def log(self, name):
        "imprime texto 'name' en una ventana TxtCtrl 'self.logger'"
        self.logger.AppendText(str(name)+'\n')
    
    def read_params(self):
        "lee parametros en la gui"
        self.dir_maldi = self.dirwin.dmaldi.tc.GetValue()
        self.dir_data  = self.dirwin.ddata.tc.GetValue()
        self.dir_report= self.dirwin.drprt.tc.GetValue()
        self.dir_htms  = self.dirwin.dhtms.tc.GetValue()
        self.onefile = self.serverwin.chkonefl.IsChecked()
        #
        self.server = self.serverwin.chcsrvr.GetSelection()
        self.on_useserver(None)
        #
        datlw = self.pw.leftw.properties
        for key in datlw:
            self.value[key]=datlw[key].GetValue()
            self.log(key + '---->' + self.value[key])
            
    def on_open_in_xls(self, evt):
        trgtpth = self.tc.location.GetValue()
        os.system(r'start EXCEL.EXE '+ trgtpth)
        
    def on_useserver(self, evt):
        self.useserver = self.serverwin.chkusrvr.IsChecked()
        self.dirwin.dmaldi.tc.Enable(self.useserver)
        self.dirwin.dmaldi.bsel.Enable(self.useserver)
        self.dirwin.ddata.tc.Enable(eval('not self.useserver'))
        self.dirwin.ddata.bsel.Enable(eval('not self.useserver'))
    
    def on_unk(self,evt):
        aviso('No implementado %s' % evt.GetEventType())
    
    def on_send(self, evt):
        print 'estoy en send'
        
    def on_check(self, evt):
        print 'on_check_server'
                         
    def on_select(self, evt):
        objecto = evt.GetEventObject()
        name = objecto.GetName()
        win = getattr(self.dirwin, name)
        acepta_file_entry = (name in ['dmaldi','ddata']) 
        if self.serverwin.chkonefl.IsChecked() and acepta_file_entry:
            dir = False
        else:
            dir = True
        #
        print get_filepath(win.tc, isdir=dir)
    
    def on_documents(self, evt):
        pass
    
    def on_user(self, evt):
        pass
    
    def on_settings(self, evt):
        pass
     
    def on_set_memosite(self, evt):
        pass
        
    def on_help(self, evt):
        helpframe = HelpFrame(None)
        helpframe.Show()

    def OnCloseWindow(self, event):
        self.Destroy()


if __name__ == '__main__':
    from xmr_constants import DIRQUIM   
    app = wx.PySimpleApp()
    BaseFrame(DIRQUIM, title=TITULO).Show()
    app.MainLoop()
