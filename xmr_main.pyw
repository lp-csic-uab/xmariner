#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_main_15
(xmr_15) 28 de abril de 2008
Este modulo siempre sube a la version mas alta
"""
#
import os
import wx
from xmr_cls_baseframe_06 import BaseFrame
from xmr_cls_reportpar_03 import ReportParamsFrame
from xmr_cls_notefact_11 import UserFrame
from xmr_cls_notefact_11 import NoteFactFrame
from xmr_funcs_11 import runapp, get_query, get_filepath
from xmr_funcs_11 import aviso
from xmr_constants import PPSERVER
#
TITULO = ' XMAR vs.1.5'
#
#
def set_globals():
    "genera valores globales que sobregraban los importados de xmr_constants"
    for linea in open('xmr_config_search', 'r'):
        if linea.startswith('#'): continue
        key, value = linea.strip().split('=')
        if ';;' in value: value = value.split(';;')  #para listas
        globals()[key]=value
#
#
class ReportParamsXmar(ReportParamsFrame):
    ""
    exist = False
    def __init__(self, parent, formatdata, *args, **kargs):
        ReportParamsFrame.__init__(self, parent, *args, **kargs)
        ReportParamsXmar.exist = True
        self.parent = parent
        self.default = formatdata
        self.setdata(formatdata)
        self.Bind(wx.EVT_BUTTON, self.on_bt_apply, self.bt_apply)
        self.Bind(wx.EVT_BUTTON, self.on_bt_exit, self.bt_exit)
        self.Bind(wx.EVT_BUTTON, self.on_bt_dflt, self.bt_dflt)
        self.Bind(wx.EVT_CLOSE, self.on_close)
    
    def on_close(self, evt):
        ReportParamsXmar.exist = False
        self.Destroy()
        
    def on_bt_dflt(self, evt):
        self.setdata(self.default)
    
    def setdata(self, data):
        self.sp_ion.SetValue(data[0])
        self.sp_ssz.SetValue(data[1])
        self.sp_cnd.SetValue(data[2])
        self.sp_mws.SetValue(data[3])
        self.sp_mdf.SetValue(data[4])
    
    def getdata(self):
        niones = self.sp_ion.GetValue()
        sectsize = self.sp_ssz.GetValue()
        candidatos = self.sp_cnd.GetValue()
        minmowse = self.sp_mws.GetValue()
        maxdif = self.sp_mdf.GetValue()
        return (niones, sectsize, candidatos, minmowse, maxdif)
        
    def on_bt_exit(self, evt):
        self.Destroy()
    
    def on_bt_apply(self, evt):
        data = self.getdata()
        self.parent.formatdata = data
        
    #def on_close(self, evt):
    #    pass
#
#
class XmarGui(BaseFrame):
    ""
    def __init__(self, dirbase, title):
        BaseFrame.__init__(self, dirbase, title)
        set_globals()
        self.source = []            #files a correr
        self.groups = {}                #diccionario de usuarios
        self.formatdata = (5, 10, 2, 3, 3)     #parametros de ReportParamsXmar
        self.dir = dirbase
        self.memosite = None
        self.make_dict_servers()
        self.get_additional_params()
        
    def make_dict_servers(self):
        """En configuracion los servidores adicionales se leen como una lista
        de tuplas de dos elementos: (servername, serverurl)
        Esta funcion mete estos datos en PPSERVER
        """
        globaldict = globals()
        CUSTOMSERVERS = [globaldict[server] for server in globaldict.keys()
                         if server.startswith('PPSERVER_USER')] 
        #
        for item in CUSTOMSERVERS:
            if item is not None:
                PPSERVER[item[0]] = item[1]
            self.ppserver = PPSERVER
            self.serverwin.chcsrvr.SetItems(PPSERVER.keys())
            self.serverwin.chcsrvr.SetStringSelection('local')
        
    def on_send(self, evt):
        ""
        self.read_params()
        self.get_additional_params()
        #
        servidor = self.serverwin.chcsrvr.GetValue()
        afilter = self.serverwin.chkfilter.IsChecked()
        if afilter: afilter = (self.formatdata[0], 10*self.formatdata[1])
        minmowse = 10**self.formatdata[3]
        maxdif = 10**self.formatdata[4]
        #
        self.log('run servidor='+str(self.useserver))
        if self.useserver:
            self.useserver = self.ppserver[servidor]
            self.dir = self.dir_maldi
        else:
            self.dir = self.dir_data
       
        self.organiza_dir()
        for user in self.groups:
            flpth = runapp(self.groups[user], self.value,
                           self.formatdata[2], minmowse, maxdif,
                           self.dir_htms, self.dir_report,
                           self.useserver, afilter,
                           self.log)
            if not flpth: continue
            self.tc.ie.LoadUrl(flpth)
            if self.tc.location.FindString(flpth) == -1:
                self.tc.location.Append(flpth)
        
    def get_additional_params(self):
        """Los parametros en diccionario value se envian a la query (TODOS)
        Asi que no se debe utilizar este diccionario para transportar cosas
        y de ahi que se utilize self.filter
        Algunos de estos parametros deberian ir a baseframe...
        'mowse_on': '1',
        """
        #
        piso = self.pw.centerw.properties['pI  full range'].IsChecked()
        mwght = self.pw.centerw.properties['Mw full range'].IsChecked()
        #
        self.value['full_pi_range'] = str(int(piso))
        self.value['full_mw_range'] = str(int(mwght))
        self.value['mowse_on'] = '1'    
        self.value['parent_masses'] = """676.3718
825.4571
1017.5370
1026.5964
1040.6121
1105.4550
1196.7132
1218.5796"""
        
        for item in ['full_pi_range',
                     'full_mw_range', 'mowse_on']:
            self.log('%s ----> %s' %(item, self.value[item]))
        
    def organiza_dir(self):
        ""
        self.groups = {}
        if not self.dir or not os.path.exists(self.dir):
            return
        #
        if os.path.isfile(self.dir):
            basename = os.path.basename(self.dir)
            name = basename.split('_')[0]
            self.groups[name] = [self.dir]
            return
        # 
        if self.onefile:
            aviso("%s\n%s\n%s" %("     Selecciona una file",
                                 "     o desmarca 'One file'",
                                 "Tienes seleccionado un directorio"))
            return
        
        for arch in os.listdir(self.dir):
            basename = os.path.basename(arch)
            name = basename.split('_')[0]
            arch = self.dir + os.sep + arch
            if name in self.groups:
                self.groups[name].append(arch)
            else:
                self.groups[name] = [arch]
       
    def on_documents(self, evt):
        ""
        resultado = self.tc.location.GetValue()
        if resultado.endswith('.htm') and not resultado.startswith('http:'):
            arch = resultado
        else:
            self.log("""Para lectura automatizada de los datos de report
y usuario debe haber un archivo de resultados tipo .htm seleccionado.
El archivo no puede estar en un servidor http"""
                    )
            arch = ''
            
        NoteFactFrame(None, arch, self.memosite, self.tc).Show()
         
    def on_user(self, evt):
        UserFrame(None, self.memosite).Show()
    
    def on_settings(self, evt):
        ""
        if ReportParamsXmar.exist: return
        ReportParamsXmar(self, self.formatdata).Show()
        
    def on_set_memosite(self, evt):
        ""
        path, bol = get_filepath(None, isdir=True)
        if path and os.path.isdir(path): self.memosite = path
        self.log('memosite set to '+ str(self.memosite))
       
    def on_check(self, evt):
        ""
        servidor = self.serverwin.chcsrvr.GetValue()
        self.log('run servidor=' + str(self.useserver))
        server = self.ppserver[servidor]
        
        head = '\n Ask to %s' % server
        box = 23*'*'
        msg = '\n** Recibo = %-15s  **\n** %-23s **\n' % get_query('', server)
        msg = head + '\n' + box + msg + box + '\n'
        self.log(msg)
      
     
if __name__ == "__main__":
    
    from xmr_constants import DIRQUIM
    app = wx.PySimpleApp()
    XmarGui(DIRQUIM, title=TITULO).Show()
    app.MainLoop()
    