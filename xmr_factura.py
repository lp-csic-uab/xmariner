#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
xmr_factura_01
(xmr_15)29 de abril de 2008
"""
#
from pxword import Word

class Factura(Word):
    
    def __init__(self, archive=None, visible=True):
        Word.__init__(self, archive, visible)
        
    def haz_factura(self, datos, **kargs):
        for key, item in datos.iteritems():
            key = '.%s.' %key
            self.chRemplAll(key, item)
        
if __name__ == '__main__':
    
    from xmr_constants import FORM_FACTURAS
    archivo = FORM_FACTURAS
    total = 13
    mapeo =  5
    resto = total - mapeo
    coste = 5 * 28 + resto * 25.2
    #
    datos = dict(
    UNAME   =   'Carme Quero',
    DCENTRO =   'Dpto. Qu�mica Org�nica',
    ADD1    =   'Centro de Investigaci�n y Desarrollo (CID)',
    ADD2    =   'C/ Jordi Girona, 18-26',
    UTREAT  =   'Dra',
    CP      =   '08034',
    CITY    =   'Barcelona',
    CITY2   =   'Bellaterra',
    NIF     =   'Q2818002D',
    DFACT   =   'Mayo 2007',
    FF      =   'Agosto 2007',
    REFINT  =   '113/2007',
    ANALYSIS=   "Peptide Mass Fingerprinting by MS-MALDI TOF",
    TOTM    =   str(total),
    MM      =   str(mapeo),
    MS      =   str(resto),
    IMPORT  =   str(coste)
    )
    #
    #print datos
    factura = Factura(archivo)
    factura.haz_factura(datos)
    factura.saveas('c:\\Toto.rtf', 6)
    factura.close()
    factura.quit()